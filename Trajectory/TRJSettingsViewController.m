//
//  TRJScreen5ViewController.m
//  Trajectory
//
//  Created by Brock Hardman on 3/21/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import "TRJSettingsViewController.h"

@interface TRJSettingsViewController ()

@end

@implementation TRJSettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor orangeColor];
}

@end
