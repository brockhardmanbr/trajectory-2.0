//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "TRJUtilityCircleMaskView.h"

IB_DESIGNABLE

@interface TRJUtilityCircleImageView : TRJUtilityCircleMaskView

@property (nonatomic, strong) IBInspectable UIImage *image;

- (void)updateImage;
- (void)setImageURL:(NSURL *)URL;

@end
