//
//  TRJProfile.m
//  Trajectory
//
//  Created by Brock Hardman on 4/26/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import "TRJProfile.h"

#define safeSet(d,k,v) if (v) d[k] = v;

@implementation TRJProfile

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
        self._id = dictionary[@"_id"];
        self.emailAddress = dictionary[@"emailAddress"];
        self.firstName = dictionary[@"firstName"];
        self.lastName = dictionary[@"lastName"];
        self.imageId = dictionary[@"imageId"];
    }
    
    return self;
}

- (NSDictionary *)toDictionary
{
    NSMutableDictionary* jsonable = [NSMutableDictionary dictionary];
    safeSet(jsonable, @"_id", self._id);
    safeSet(jsonable, @"emailAddress", self.emailAddress);
    safeSet(jsonable, @"firstName", self.firstName);
    safeSet(jsonable, @"lastName", self.lastName);
    safeSet(jsonable, @"imageId", self.imageId);

    return jsonable;
}

@end
