//
//  TRJActiveControllersProvider.h
//  Trajectory
//
//  Created by Brock Hardman on 3/31/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import "TRJActiveControllersProviderProtocol.h"

typedef void(^TRJActiveControllersCompletionBlock)(void);

typedef NS_OPTIONS(NSInteger, TRJActiveControllersProviderServerType)
{
    TRJActiveControllersProviderServerTypeNodeJS,
    TRJActiveControllersProviderServerTypeParse
};

@interface TRJActiveControllersProvider : NSObject <TRJActiveControllersProviderProtocol>

- (instancetype)initWithJsonURL:(NSString *)url serverType:(TRJActiveControllersProviderServerType)serverType withCompletionBlock:(TRJActiveControllersCompletionBlock)completionBlock;

@end
