
//
//  TRJPostContent.m
//  Trajectory
//
//  Created by Brock Hardman on 5/22/14.
//  Copyright (c) 2014 Bottle Rocket, LLC. All rights reserved.
//

#import "TRJPostContent.h"

@implementation TRJPostContent

+(NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"identifier": @"id",
             @"contentTypeString": @"contentType",
             @"filePath": @"filepath",
//             @"fileSize": @"filesize",
             @"postText": @"text",
//             @"shoutoutUsers": @"involvedUsers"
            };
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"\nPostContentId:%d\nPostText: %@\n\n\n",(int)self.identifier, self.postText];
}

- (TRJPostContentType)contentType
{
    if ([self.contentTypeString isEqualToString:@"emoticon"])
    {
        return TRJPostContentTypeEmoticon;
    }
    
    if ([self.contentTypeString isEqualToString:@"text"])
    {
        return TRJPostContentTypeText;
    }
    
    if ([self.contentTypeString isEqualToString:@"image/jpeg"] || [self.contentTypeString isEqualToString:@"image/png"])
    {
        return TRJPostContentTypeImage;
    }
    
    if ([self.contentTypeString isEqualToString:@"comment"])
    {
        return TRJPostContentTypeComment;
    }
    
    if ([self.contentTypeString isEqualToString:@"shoutout"])
    {
        return TRJPostContentTypeShoutOut;
    }
    
    if ([self.contentTypeString isEqualToString:@"location"])
    {
        return TRJPostContentTypeLocation;
    }
    
    return 0;
}

@end
