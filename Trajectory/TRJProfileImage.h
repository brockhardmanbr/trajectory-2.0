//
//  TRJProfileImage.h
//  Trajectory
//
//  Created by Brock Hardman on 4/29/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TRJProfileImage : NSObject

@property (nonatomic, copy) UIImage *image;
@property (nonatomic, copy) NSString *imageId;

@end
