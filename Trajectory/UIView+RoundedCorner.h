//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (RoundedCorner)

- (void)viewWithRoundedCorners;
- (void)viewWithRoundedCornersWithRadius:(CGFloat)radius;

@end
