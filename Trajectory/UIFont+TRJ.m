//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "UIFont+TRJ.h"

@implementation UIFont (TRJ)

+ (UIFont *)pet_openSansFontWithSize:(CGFloat)size
{
    return [UIFont fontWithName:@"OpenSans" size:size];
}

@end
