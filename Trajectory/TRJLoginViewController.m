//
//  TRJLoginViewController.m
//  Trajectory
//
//  Created by Brock Hardman on 4/30/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import "TRJLoginViewController.h"
#import <Parse/Parse.h>

@interface TRJLoginViewController ()
@property (weak, nonatomic) IBOutlet UIView *loginContainerView;
@property (weak, nonatomic) IBOutlet UILabel *welcomeLabel;
@property (weak, nonatomic) IBOutlet UILabel *bottomMessageLabel;
@property (weak, nonatomic) IBOutlet UIButton *launchButton;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UIVisualEffectView *visualEffectView;
@property (nonatomic, copy) TRJLoginViewControllerCompletionBlock completionBlock;
@property (nonatomic) TRJLoginControllerProviderServiceType service;
@property (nonatomic) BOOL fieldValidationErrorFlag;

@end

@implementation TRJLoginViewController

- (void)configureWithService:(TRJLoginControllerProviderServiceType)service withCompletionBlock:(TRJLoginViewControllerCompletionBlock)completionBlock
{
    self.completionBlock = completionBlock;
    self.service = service;
}

+ (NSString *)storyboardIdentifier
{
    return NSStringFromClass([self class]);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self addShadowToView:self.loginContainerView];
    self.launchButton.layer.cornerRadius = 5;
}

- (void)addShadowToView:(UIView *)view
{
    CALayer *layer = [view layer];
    [layer setMasksToBounds:NO];
    [layer setRasterizationScale:[[UIScreen mainScreen] scale]];
    [layer setShouldRasterize:YES];
    [layer setShadowColor:[[UIColor blackColor] CGColor]];
    [layer setShadowOffset:CGSizeMake(5.0f, 5.0f)];
    [layer setShadowRadius:10.0f];
    [layer setShadowOpacity:0.5f];
    [layer setShadowPath:[[UIBezierPath bezierPathWithRoundedRect:view.bounds cornerRadius:layer.cornerRadius] CGPath]];
}

- (IBAction)launchButtonTapped:(UIButton *)sender
{
    self.fieldValidationErrorFlag = NO;
    [self loginWithAppropriateService];
}

- (void)loginWithAppropriateService
{
    //[self runErrorChecks];
    
    if (!self.fieldValidationErrorFlag)
    {
        if (self.service == TRJLoginControllerProviderServiceTypeParse)
        {
            [self loginWithParse];
        }
    }
}

- (void)runErrorChecks
{
    if ([self.usernameTextField.text length] == 0)
    {
        self.fieldValidationErrorFlag = YES;
    }
    
    if ([self.passwordTextField.text length] == 0)
    {
        self.fieldValidationErrorFlag = YES;
    }
}

- (void)loginWithParse
{
    //Comment out until ready for use
    //NSString *username = self.usernameTextField.text;
    //NSString *password = self.passwordTextField.text;
    
    [PFUser logInWithUsernameInBackground:@"brock.hardman" password:@"rocketeer1" block:^(PFUser *user, NSError *error) {
        if (error)
        {
            [self userDidFailLogin];
            NSLog(@"Error: %@", error);
        }
        else
        {
            [self userDidLoginSuccessfully];
        }
    }];
}

- (void)userDidFailLogin
{
    
}

- (void)userDidLoginSuccessfully
{
    if (self.completionBlock) self.completionBlock();
}

@end
