//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "UIScrollView+KeyboardAdjusting.h"

@interface TRJUtilityKeyboardAdjustingTableView : UITableView <UITextFieldDelegate, UITextViewDelegate>

- (BOOL)focusNextTextField;
- (void)scrollToActiveTextField;

@end