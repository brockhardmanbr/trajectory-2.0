//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "TRJUtilityKeyboardAdjustingScrollView.h"

@interface TRJUtilityKeyboardAdjustingScrollView () <UITextFieldDelegate, UITextViewDelegate>

@end

@implementation TRJUtilityKeyboardAdjustingScrollView

#pragma mark - Setup/Teardown

- (void)setup
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyboardAdjusting_keyboardWillShow:) name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyboardAdjusting_keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(scrollToActiveTextField) name:UITextViewTextDidBeginEditingNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(scrollToActiveTextField) name:UITextFieldTextDidBeginEditingNotification object:nil];
}

-(instancetype)initWithFrame:(CGRect)frame
{
    if (!(self = [super initWithFrame:frame])) return nil;
    [self setup];
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setup];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    [self KeyboardAdjusting_updateContentInset];
}

-(void)setContentSize:(CGSize)contentSize
{
    [super setContentSize:contentSize];
    [self KeyboardAdjusting_updateFromContentSizeChange];
}

- (void)contentSizeToFit
{
    self.contentSize = [self KeyboardAdjusting_calculatedContentSizeFromSubviewFrames];
}

- (BOOL)focusNextTextField
{
    return [self KeyboardAdjusting_focusNextTextField];
}

- (void)scrollToActiveTextField
{
    [self KeyboardAdjusting_scrollToActiveTextField];
}

#pragma mark - Responders, events

-(void)willMoveToSuperview:(UIView *)newSuperview
{
    [super willMoveToSuperview:newSuperview];
    
    if (!newSuperview)
    {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(KeyboardAdjusting_assignTextDelegateForViewsBeneathView:) object:self];
    }
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[self KeyboardAdjusting_findFirstResponderBeneathView:self] resignFirstResponder];
    [super touchesEnded:touches withEvent:event];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (![self focusNextTextField])
    {
        [textField resignFirstResponder];
    }
    return YES;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(KeyboardAdjusting_assignTextDelegateForViewsBeneathView:) object:self];
    [self performSelector:@selector(KeyboardAdjusting_assignTextDelegateForViewsBeneathView:) withObject:self afterDelay:0.1];
}

@end
