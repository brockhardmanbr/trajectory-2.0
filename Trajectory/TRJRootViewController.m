//
//  ViewController.m
//  Trajectory
//
//  Created by Brock Hardman on 3/21/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import "TRJRootViewController.h"
#import "TRJActiveControllersProviderProtocol.h"
#import "TRJLoginViewController.h"
#import <Parse/PFUser.h>

static CGFloat const kMainPinchGestureMaxScale = 1.0f;
static CGFloat const kMainPinchGestureMinScale = 0.5f;

typedef void(^ControllersCompletionBlock)(void);

@interface TRJRootViewController ()<UIScrollViewDelegate, UIGestureRecognizerDelegate, TRJActiveViewControllerRefreshProtocol>

@property (weak, nonatomic) IBOutlet UIVisualEffectView *visualEffectsView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *bottomNavView;
@property (nonatomic, strong) NSArray *controllers;
@property (nonatomic, strong) NSArray *activeControllers;
@property (nonatomic, strong) NSMutableArray *mutableActiveControllers;
@property (nonatomic, strong) NSMutableArray *tapGestures;
@property (nonatomic, strong) UIView *view1;
@property (nonatomic, strong) UIView *view2;
@property (nonatomic, strong) UIView *view3;
@property (nonatomic, strong) UIView *view4;
@property (nonatomic, strong) UIView *view5;
@property (nonatomic) BOOL isZoomed;
@property (nonatomic) NSInteger currentControllerIndex;
@property (nonatomic, strong) id<TRJActiveControllersProviderProtocol> controllerProvider;
@property (nonatomic, strong) id<TRJLoginControllerProtocol> dataProviderLoginController;
@property (nonatomic, strong) UIViewController *loginViewControllerFromDataProvider;
@property (nonatomic) TRJLoginControllerProviderServiceType service;
@property (nonatomic, strong) PFUser *user;

@end

@implementation TRJRootViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.service = TRJLoginControllerProviderServiceTypeParse;
    
    [self retrieveActiveControllers];
    [self buildScrollViewWithActiveControllers];
    [self addActiveViewControllers];
    [self buildBottomNav];
    
    self.currentControllerIndex = 0;
    
    UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(userDidPinch:)];
    [self.scrollView addGestureRecognizer:pinchGesture];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self loginIfNeededWithDataProvider:self.service];
}

#pragma mark - Internal

- (void)loginIfNeededWithDataProvider:(TRJLoginControllerProviderServiceType)service
{
    [self retrieveLoginProvider];
    
    if (![self userIsLoggedIn])
    {
        if ([self.dataProviderLoginController conformsToProtocol:@protocol(TRJLoginControllerProtocol)])
        {
            if ([self.dataProviderLoginController isKindOfClass:[UIViewController class]])
            {
                self.loginViewControllerFromDataProvider = (UIViewController *)self.dataProviderLoginController;
                [self addChildViewController:self.loginViewControllerFromDataProvider];
                [self.loginViewControllerFromDataProvider.view setAlpha:0];
                [self.view addSubview:self.loginViewControllerFromDataProvider.view];
                [self.loginViewControllerFromDataProvider didMoveToParentViewController:self];
                
                [UIView animateWithDuration:0.5 animations:^{
                    [self.loginViewControllerFromDataProvider.view setAlpha:1];
                }];
            }
        }
    }
}

- (BOOL)userIsLoggedIn
{
    if ([PFUser currentUser])
    {
        return YES;
    }
    
    return NO;
}

- (void)retrieveLoginProvider
{
    ControllersCompletionBlock block = ^(void) {
        
        [UIView animateWithDuration:0.5 animations:^{
            [self.loginViewControllerFromDataProvider.view setAlpha:0];
        } completion:^(BOOL finished) {
            [self.loginViewControllerFromDataProvider willMoveToParentViewController:nil];
            [self.loginViewControllerFromDataProvider.view removeFromSuperview];
            [self.loginViewControllerFromDataProvider removeFromParentViewController];
        }];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kUserDidLoginNotification object:nil];
    };
    
    NSInteger integerValue = TRJLoginControllerProviderServiceTypeParse;
    NSNumber *numberValue = [NSNumber numberWithInteger:integerValue];
    self.dataProviderLoginController = [[BRAFServiceLocator sharedLocator] serviceForProtocol:@protocol(TRJLoginControllerProtocol) context:kLoginControllerContext parameters:@[numberValue, [block copy]]];
}

- (void)retrieveActiveControllers
{
    ControllersCompletionBlock block = ^(void) {
        
    };
    
    self.controllerProvider = [[BRAFServiceLocator sharedLocator] serviceForProtocol:@protocol(TRJActiveControllersProviderProtocol) context:kActiveControllersContext parameters:@[@"https://www.dropbox.com/s/ifxf5qp4n4hdzuv/activeViewControllers.json?dl=0", [block copy]]];
    self.controllers = [self.controllerProvider controllers];
}

- (void)buildScrollViewWithActiveControllers
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    self.mutableActiveControllers = [[NSMutableArray alloc] init];
    
    for (NSUInteger i = 0; i < [self.controllers count]; i++)
    {
        CGFloat originX = i * screenBounds.size.width;
        CGRect frame = CGRectMake(originX, screenBounds.origin.y, screenBounds.size.width, self.scrollView.frame.size.height);
        UIView *backingView = [[UIView alloc] initWithFrame:frame];
        [self.scrollView addSubview:backingView];
        
        [self addActiveControllerToView:backingView withViewControllerDictionary:[self.controllers objectAtIndex:i]];
    }
    
    CGFloat contentWidth = screenBounds.size.width * [self.controllers count];
    [self.scrollView setContentSize:CGSizeMake(contentWidth, self.scrollView.frame.size.height)];
}

- (void)addActiveViewControllers
{
    self.activeControllers = [NSArray arrayWithArray:self.mutableActiveControllers];
}

- (void)addActiveControllerToView:(UIView *)view withViewControllerDictionary:(NSDictionary *)viewControllerDictionary
{
    if ([self.controllers count])
    {
        CGRect screenBounds = [[UIScreen mainScreen] bounds];
        
        if ([viewControllerDictionary isKindOfClass:[NSDictionary class]])
        {
            NSString *navTitle = [viewControllerDictionary objectForKey:@"navTitle"];
            NSString *navImage = [viewControllerDictionary objectForKey:@"navImage"];
            NSString *viewControllerClassName = [viewControllerDictionary objectForKey:@"viewControllerClassName"];
            NSString *storyboardName = [viewControllerDictionary objectForKey:@"storyboardName"];
            
            TRJActiveViewController *activeController = [[UIStoryboard storyboardWithName:storyboardName bundle:nil] instantiateViewControllerWithIdentifier:viewControllerClassName];
            activeController.refreshDelegate = self;
            
            BOOL isValidViewController = [activeController conformsToProtocol:@protocol(TRJActiveViewControllerProtocol)];
            
            if (isValidViewController)
            {
                [activeController configureWithNavigationTitle:navTitle navigationImage:navImage];
                [self addChildViewController:activeController];
                [self.view addSubview:activeController.view];
                [activeController didMoveToParentViewController:self];
                [activeController.view setFrame:screenBounds];
                [view addSubview:activeController.view];
                [self.mutableActiveControllers addObject:activeController];
            }
        }
    }
}

- (void)buildBottomNav
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat navButtonWidth = screenRect.size.width / [self.activeControllers count];
    NSInteger counter = 0;
    
    for (TRJActiveViewController *activeViewController in self.activeControllers)
    {
        CGFloat beginX = counter * navButtonWidth;
        UIView *navButtonView = [[UIView alloc] initWithFrame:CGRectMake(beginX, 0, navButtonWidth, self.bottomNavView.frame.size.height)];
        [navButtonView setBackgroundColor:[UIColor clearColor]];
        
        UIButton *navButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, navButtonWidth, self.bottomNavView.frame.size.height)];
        navButton.tag = counter;
        [navButton addTarget:self action:@selector(navButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [navButton setTitle:activeViewController.navigationTitle forState:UIControlStateNormal];
        [navButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [navButton setTitleEdgeInsets:UIEdgeInsetsMake(self.bottomNavView.frame.size.height - 18, 0, 0, 0)];
        [navButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:10]];
        [navButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [navButton setBackgroundColor:[UIColor clearColor]];
        [navButtonView addSubview:navButton];
        
        UIImageView *buttonImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:activeViewController.navigationImage]];
        [buttonImageView setCenter:CGPointMake(navButtonWidth / 2, buttonImageView.frame.size.height / 2 + 3)];
        [buttonImageView setBackgroundColor:[UIColor clearColor]];
        [navButtonView addSubview:buttonImageView];
        
        [self.bottomNavView addSubview:navButtonView];
        counter++;
    }
}

- (void)setControllersToMinScaleWithDuration:(NSTimeInterval)timeInterval
{
    for (UIViewController *controller in self.activeControllers)
    {
        TRJActiveViewController *activeController = (TRJActiveViewController *)controller;
        [activeController.view setClipsToBounds:NO];
        
        [UIView animateWithDuration:timeInterval animations:^{
            CGAffineTransform transform = CGAffineTransformMakeScale(kMainPinchGestureMinScale, kMainPinchGestureMinScale);
            activeController.view.transform = transform;
            UIView *floatingTitleView = [[UIView alloc] initWithFrame:CGRectMake(activeController.view.bounds.origin.x, activeController.view.bounds.origin.y - 100, activeController.view.bounds.size.width, 50)];
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:floatingTitleView.bounds];
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.text = [activeController.navigationTitle uppercaseString];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:25];
            [floatingTitleView addSubview:titleLabel];
            [controller.view addSubview:floatingTitleView];
        } completion:^(BOOL finished) {
            [self addShadowToView:controller.view];
            [self addTapGestureToView:controller.view];
        }];
    }
    
    self.isZoomed = YES;
}

- (void)setControllersToMaxScaleWithDuration:(NSTimeInterval)timeInterval
{
    for (UIViewController *controller in self.activeControllers)
    {
        [UIView animateWithDuration:timeInterval animations:^{
            CGAffineTransform transform = CGAffineTransformMakeScale(kMainPinchGestureMaxScale, kMainPinchGestureMaxScale);
            controller.view.transform = transform;
        } completion:^(BOOL finished) {
            [self removeShadowFromView:controller.view];
            [self removeTapGestureFromView:controller.view];
            [controller.view setClipsToBounds:YES];
        }];
    }
    
    self.isZoomed = NO;
}

- (void)addShadowToView:(UIView *)view
{
    CALayer *layer = [view layer];
    [layer setMasksToBounds:NO];
    [layer setRasterizationScale:[[UIScreen mainScreen] scale]];
    [layer setShouldRasterize:YES];
    [layer setShadowColor:[[UIColor blackColor] CGColor]];
    [layer setShadowOffset:CGSizeMake(5.0f, 5.0f)];
    [layer setShadowRadius:10.0f];
    [layer setShadowOpacity:0.5f];
    [layer setShadowPath:[[UIBezierPath bezierPathWithRoundedRect:view.bounds cornerRadius:layer.cornerRadius] CGPath]];
}

- (void)removeShadowFromView:(UIView *)view
{
    CALayer *layer = [view layer];
    [layer setShadowColor:nil];
    [layer setShadowOffset:CGSizeZero];
    [layer setShadowRadius:0.0f];
    [layer setShadowOpacity:0.0f];
    [layer setShadowPath:nil];
}

- (void)addTapGestureToView:(UIView *)view
{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userDidTap:)];
    [view addGestureRecognizer:tapGesture];
    [self.tapGestures addObject:tapGesture];
}

- (void)removeTapGestureFromView:(UIView *)view
{
    for (UIGestureRecognizer *recognizer in view.gestureRecognizers)
    {
        if ([self.tapGestures containsObject:recognizer])
        {
            [view removeGestureRecognizer:recognizer];
            [self.tapGestures removeObject:recognizer];
        }
    }
}

- (void)userDidTap:(UITapGestureRecognizer *)tap
{
    if (self.isZoomed)
    {
        [self setControllersToMaxScaleWithDuration:0.2f];
    }
}

- (void)updateControllersWithScale:(CGFloat)scale
{
    for (TRJActiveViewController *controller in self.activeControllers)
    {
        CGAffineTransform transform = CGAffineTransformMakeScale(scale, scale);
        controller.view.transform = transform;
    }
}

- (CGFloat)getUpdatedScaleWithScale:(CGFloat)scale
{
    CGFloat updatedScale = scale;
    if (self.isZoomed)
    {
        updatedScale = updatedScale - kMainPinchGestureMinScale;
    }
    
    //This check will keep the user from zooming past max scale as it causes weird behavior
    return MIN(updatedScale, kMainPinchGestureMaxScale);
}

- (void)userDidPinch:(UIPinchGestureRecognizer *)pinch
{
    CGFloat scaleToUse = [self getUpdatedScaleWithScale:pinch.scale];
    
    if (pinch.state == UIGestureRecognizerStateChanged)
    {
        [self updateControllersWithScale:scaleToUse];
    }
    else if (pinch.state == UIGestureRecognizerStateEnded)
    {
        CGFloat scaleLowerThreshold = (1.0f - ((kMainPinchGestureMaxScale - kMainPinchGestureMinScale) / 2.0f));
        
        if (scaleToUse < scaleLowerThreshold)
        {
            [self setControllersToMinScaleWithDuration:0.2f];
        }
        else
        {
            [self setControllersToMaxScaleWithDuration:0.2f];
        }
    }
}

- (void)navButtonTapped:(UIButton *)sender
{
    if (self.currentControllerIndex != sender.tag)
    {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat newOffsetX = screenRect.size.width * sender.tag;
        
        if (self.isZoomed)
        {
            [UIView animateWithDuration:0.6f animations:^{
                [self.scrollView setContentOffset:CGPointMake(newOffsetX, 0)];
            } completion:^(BOOL finished) {
                [self setControllersToMaxScaleWithDuration:0.4f];
                self.currentControllerIndex = sender.tag;
            }];
        }
        else
        {
            [self setControllersToMinScaleWithDuration:0.4f];
            
            [UIView animateWithDuration:0.6f animations:^{
                [self.scrollView setContentOffset:CGPointMake(newOffsetX, 0)];
            } completion:^(BOOL finished) {
                [self setControllersToMaxScaleWithDuration:0.4f];
                self.currentControllerIndex = sender.tag;
            }];
        }
    }
}

#pragma mark - Active View Controller Refresh Protocol

- (void)layoutNeedsRefresh
{
    [self.view setNeedsDisplay];
}

#pragma mark - Scrollview Delegate Methods

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat currentOffsetX = scrollView.contentOffset.x;
    NSInteger page = scrollView.contentSize.width / currentOffsetX;
    self.currentControllerIndex = page;
}

@end
