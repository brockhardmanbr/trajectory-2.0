//
//  TRJParseUser.h
//  Trajectory
//
//  Created by Brock Hardman on 5/1/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TRJParseUser : NSObject

@property (nonatomic, copy) NSString *objectId;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *organizationalUnit;
@property (nonatomic, copy) NSString *position;

@end
