//
//  TRJParsePost.h
//  Trajectory
//
//  Created by Brock Hardman on 5/1/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import <Parse/PFUser.h>

@interface TRJParsePost : NSObject

@property (nonatomic, copy) NSString *objectId;
@property (nonatomic, copy) NSDate *updatedAt;
@property (nonatomic, copy) NSDate *createdAt;
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *postText;
@property (nonatomic, strong) NSArray *postImages;

@end
