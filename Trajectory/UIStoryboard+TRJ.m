//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "UIStoryboard+TRJ.h"

@implementation UIStoryboard (TRJ)

+ (UIStoryboard *)trjProfileStoryboard
{
    return [UIStoryboard storyboardWithName:@"TRJProfile" bundle:nil];
}

@end
