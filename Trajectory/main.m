//
//  main.m
//  Trajectory
//
//  Created by Brock Hardman on 3/21/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
