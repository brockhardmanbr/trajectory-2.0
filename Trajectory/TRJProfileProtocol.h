//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TRJProfileProtocol <NSObject>

- (NSString *)shortDisplayName;
- (NSURL *)profileImageURL;

@end
