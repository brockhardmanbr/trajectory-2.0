//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "TRJPostsConfiguration.h"

@implementation TRJPostsConfiguration

+(NSDictionary *)JSONKeyPathsByPropertyKey
{
	return @{
			 @"baseURLString" : @"base_url",
			 @"allPostsEndpoint" : @"all_posts",
			 @"singleMomentEndpoint" : @"single_post",
			 @"createMomentEndpoint" : @"create",
			 @"deleteMomentEndpoint" : @"delete",
			 @"contentEndpoint" : @"content",
             @"metadataEndpoint" : @"metadata"
			 };
}

@end
