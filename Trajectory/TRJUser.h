//
//  TRJUserObject.h
//  Trajectory
//
//  Created by Michael O'Brien on 5/21/14.
//  Copyright (c) 2014 Bottle Rocket, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

@interface TRJUser : MTLModel <MTLJSONSerializing>

@property (nonatomic) NSInteger identifier;
@property (nonatomic, copy) NSString *emailAddress;
@property (nonatomic, copy) NSString *firstName;
@property (nonatomic, copy) NSString *lastName;
@property (nonatomic, copy) UIImage *profileImage;
@property (nonatomic, copy) NSString *profileFileURL;

+ (TRJUser *)currentUser;
+ (void)setCurrentUser:(TRJUser *)currentUserObject;

- (NSString *)fullName;

@end
