//
//  TRJActiveControllersProviderProtocol.h
//  Trajectory
//
//  Created by Brock Hardman on 3/31/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

@protocol TRJActiveControllersProviderProtocol <NSObject>

- (NSArray *)controllers;

@end
