//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface TRJProfileConfiguration : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy) NSString *baseURLString;

@end
