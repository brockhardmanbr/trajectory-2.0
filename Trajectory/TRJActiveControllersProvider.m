//
//  TRJActiveControllersProvider.m
//  Trajectory
//
//  Created by Brock Hardman on 3/31/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import "TRJActiveControllersProvider.h"
#import "TRJTimelineViewController.h"
#import "TRJPostViewController.h"
#import "TRJProfileViewController.h"
#import "TRJCompanyViewController.h"
#import "TRJSettingsViewController.h"
#import "TRJNodeJSManager.h"
#import "TRJParseManager.h"

@interface TRJActiveControllersProvider()

@property (nonatomic, copy) TRJActiveControllersCompletionBlock completionBlock;
@property (nonatomic, copy) NSArray *activeControllersArray;

@end

@implementation TRJActiveControllersProvider

- (instancetype)initWithJsonURL:(NSString *)url serverType:(TRJActiveControllersProviderServerType)serverType withCompletionBlock:(TRJActiveControllersCompletionBlock)completionBlock
{
    self = [super init];
    if (self)
    {
        _completionBlock = completionBlock;
        [self configureWithServerType:serverType withURL:url];
    }
    return self;
}

- (void)configureWithServerType:(TRJActiveControllersProviderServerType)serverType withURL:(NSString *)url
{
    if (serverType == TRJActiveControllersProviderServerTypeNodeJS)
    {
        self.activeControllersArray = [[TRJNodeJSManager sharedManager] configureWithJsonURLInternal];
    }
    
    if (serverType == TRJActiveControllersProviderServerTypeParse)
    {
        self.activeControllersArray = [[TRJParseManager sharedManager] configurePostOptionsFromJSON:@"activeViewControllers"];
    }
}

- (NSArray *)controllers
{
    return self.activeControllersArray;
}

@end
