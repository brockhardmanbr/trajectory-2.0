//
//  BRAFServiceLocator.h
//  BRAFoundation
//
//  Created by Austen Green on 10/26/12.
//  Copyright (c) 2012 Bottle Rocket, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Default return block for binding a service.
 *
 *  @param parameters the supplied parameters for the service
 *
 *  @return the service being bound to the protocol
 */
typedef id (^BRAFServiceLocatorReturnBlock)(NSArray *parameters);

/**
 *  Default Context for Binding -- Used internally when registering a non-Context Protocol
 */
extern NSString * const BRAFServiceLocatorDefaultContext;

/**
 *  Maps a protocol and context to a block that creates the correct object for that protocol and context.
 
 @note This class is not thread-safe.
 */
@interface BRAFServiceLocator : NSObject

/**
 *  Shared instance of the Service Locator.
 *
 *  @return the singleton instance of the Service Locator class.
 */
+ (instancetype)sharedLocator;

#pragma mark - Locate services that respond to specific protocols

// Request an Object Instance that implements the specified Protocol
- (id)serviceForProtocol:(Protocol *)protocol;

// Request an Object Instance that implements the specified Protocol for the specified "Context"
- (id)serviceForProtocol:(Protocol *)protocol context:(NSString *)context;

// Request an Object Instance that implements the specified Protocol using the default "Context", passing in the supplied parameters
- (id)serviceForProtocol:(Protocol *)protocol parameters:(NSArray *)parameters;

// Request an Object Instance that implements the specified Protocol for the specified "Context", passing in the supplied parameters
- (id)serviceForProtocol:(Protocol *)protocol context:(NSString *)context parameters:(NSArray *)parameters;

#pragma mark - Register services that conform to specific protocols

// Register a Block to return an Instance that implements a requested Protocol
- (void)registerServiceReturnBlock:(BRAFServiceLocatorReturnBlock)block protocol:(Protocol *)protocol;

// Register a Block, as well as a "Context" to allow multiple implementors of a Protocol, to return an Instance that implements a requested Protocol
- (void)registerServiceReturnBlock:(BRAFServiceLocatorReturnBlock)block protocol:(Protocol *)protocol context:(NSString *)context;

@end
