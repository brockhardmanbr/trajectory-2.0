//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "TRJURLConfiguration.h"

@implementation TRJURLConfiguration

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
	return @{
			 @"postsConfiguration" : @"posts",
			 @"profileConfiguration" : @"profile"
			 };
}

+ (NSValueTransformer *)postsJSONTransformer
{
	return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:[TRJPostsConfiguration class]];
}

+ (NSValueTransformer *)profileJSONTransformer
{
	return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:[TRJProfileConfiguration class]];
}


@end
