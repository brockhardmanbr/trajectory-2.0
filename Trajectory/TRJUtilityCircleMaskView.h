//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+CircularMask.h"

IB_DESIGNABLE

@interface TRJUtilityCircleMaskView : UIView

- (void)updateMask;

@end
