//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TRJDateFormatterProviderProtocol <NSObject>

- (NSDateFormatter *)dateFormatterForFormatString:(NSString *)formatString;
- (NSDateFormatter *)dateFormatterForStyle:(NSDateFormatterStyle)style;

@end
