//
//  TRJScreen1ViewController.h
//  Trajectory
//
//  Created by Brock Hardman on 3/21/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import "TRJTimelineTableViewController.h"

@interface TRJTimelineViewController : TRJActiveViewController <TRJTimelineTableViewControllerDelegate>

@end
