//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

@interface UIFont (TRJ)

+ (UIFont *)pet_openSansFontWithSize:(CGFloat)size;

@end
