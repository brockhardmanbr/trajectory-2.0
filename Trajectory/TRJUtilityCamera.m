//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "TRJUtilityCamera.h"
#import "TRJUtilityBarcodeScannerViewController.h"
#import "ELCImagePickerController.h"
#import <MobileCoreServices/UTCoreTypes.h>

@interface TRJUtilityCamera()<UIImagePickerControllerDelegate, UINavigationControllerDelegate, TRJCameraBarcodeScannerViewControllerDelegate, ELCImagePickerControllerDelegate>

@property (nonatomic, strong) UIImage *image;
@property (nonatomic) TRJCameraState state;
@property (nonatomic) TRJCameraType type;
@property (nonatomic, strong) id passedObject;
@property (nonatomic, strong) UIImagePickerController *imagePicker;
@property (nonatomic, strong) ELCImagePickerController *imagePickerMultiSelect;
@property (nonatomic, strong) UIViewController *presentingViewController;
@property (nonatomic, strong) UIViewController *presentedViewController;
@property (nonatomic, copy) TRJCameraCompletionBlock singleSelectCompletionBlock;
@property (nonatomic, copy) TRJCameraMultiSelectCompletionBlock multiSelectCompletionBlock;
@property (nonatomic) BOOL allowsMultiSelect;

@end

@implementation TRJUtilityCamera

- (instancetype)initWithObject:(id)passedObject withInitialState:(TRJCameraState)state withCameraType:(TRJCameraType)type allowsMultiSelect:(BOOL)allowsMultiSelect withCompletionBlock:(id)completionBlock
{
    self = [super init];
    if (self)
    {
        _passedObject = passedObject;
        _presentingViewController = [[UIViewController alloc] init];
        _state = state;
        _type = type;
        _allowsMultiSelect = allowsMultiSelect;
        
        [self configureWithMultiSelect:allowsMultiSelect completionBlock:completionBlock];
        [self configureWithPassedObject:passedObject];
        [self configureWithNewType:type];
        [self configureWithNewState:state];
    }
    return self;
}

#pragma mark - Property Overrides and helpers

- (void)updateToNewState:(TRJCameraState)state
{
    self.state = state;
    [self configureWithNewState:state];
}

- (void)updateToNewType:(TRJCameraType)type
{
    self.type = type;
    [self configureWithNewType:type];
}

- (void)updateToNewState:(TRJCameraState)state withType:(TRJCameraType)type;
{
    [self updateToNewState:state];
    [self updateToNewType:type];
}

#pragma mark - Internal

- (void)configureWithNewType:(TRJCameraType)type
{
    if (self.state == TRJCameraStateStill)
    {
        if (type == TRJCameraTypeCamera)
        {
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
            {
                self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            }
            else
            {
                self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            }
        }
        else if (type == TRJCameraTypeRoll)
        {
            self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
        else if (type == TRJCameraTypeSavedPhotos)
        {
            self.imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        }
    }
}

- (void)configureWithNewState:(TRJCameraState)state
{
    if (state == TRJCameraStateStill)
    {
        if (self.allowsMultiSelect)
        {
            self.presentedViewController = self.imagePickerMultiSelect;
        }
        else
        {
            self.presentedViewController = self.imagePicker;
        }
    }
    else if (state == TRJCameraStateVideo)
    {
        if (self.type == TRJCameraTypeBarcodeScanner)
        {
            TRJUtilityBarcodeScannerViewController *scannerViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"petCameraBarcodeScannerViewController"];
            scannerViewController.delegate = self;
            self.presentedViewController = scannerViewController;
        }
    }
}

- (void)configureWithPassedObject:(NSObject *)passedObject
{
    if ([passedObject isKindOfClass:[UIViewController class]])
    {
        self.presentingViewController = (UIViewController *)passedObject;
    }
}

- (void)configureWithMultiSelect:(BOOL)allowMultiSelect completionBlock:(id)completionBlock
{
    if (allowMultiSelect)
    {
        [self buildMultiSelectImagePicker];
        self.multiSelectCompletionBlock = completionBlock;
    }
    else
    {
        [self buildSingleSelectImagePicker];
        self.singleSelectCompletionBlock = completionBlock;
    }
}

- (void)buildSingleSelectImagePicker
{
    self.imagePicker = [[UIImagePickerController alloc] init];
    self.imagePicker.delegate = self;
    self.imagePicker.allowsEditing = YES;
}

- (void)buildMultiSelectImagePicker
{
    ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] initImagePicker];
    
    elcPicker.maximumImagesCount = 4; //Set the maximum number of images to select to 4
    elcPicker.returnsOriginalImage = YES; //Only return the fullScreenImage, not the fullResolutionImage
    elcPicker.returnsImage = YES; //Return UIimage if YES. If NO, only return asset location information
    elcPicker.onOrder = YES; //For multiple image selection, display and return order of selected images
    elcPicker.mediaTypes = @[(NSString *)kUTTypeImage, (NSString *)kUTTypeMovie]; //Supports image and movie types
    elcPicker.imagePickerDelegate = self;
    self.imagePickerMultiSelect = elcPicker;
}

- (void)loadStillCameraAssets
{
    [self loadCameraAssetsForViewing];
}

- (void)loadVideoCameraAssets
{
    [self loadCameraAssetsForViewing];
}

- (void)loadCameraAssetsForViewing
{
    NSAssert(self.presentedViewController && self.presentingViewController, @"You must have a presenting and a presented view controller here.");
    
    [self.presentingViewController presentViewController:self.presentedViewController animated:YES completion:nil];
}

#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if (self.singleSelectCompletionBlock)
    {
        self.singleSelectCompletionBlock(info[UIImagePickerControllerEditedImage]);
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark ELCImagePickerControllerDelegate Methods

- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
{
    if (self.multiSelectCompletionBlock)
    {
        self.multiSelectCompletionBlock(info);
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Scanner Delegate methods

- (void)scanViewController:(UIViewController *)viewController didReturnErrorWithObject:(id)object
{
    NSAssert(self.presentingViewController && object, @"You must have a presenting and a presented view controller here.");
    
    if ([object isKindOfClass:[UIAlertView class]])
    {
        [(UIAlertView *)object show];
        return;
    }
    
    if ([object isKindOfClass:[UIViewController class]])
    {
        [self.presentingViewController presentViewController:(UIViewController *)object animated:YES completion:nil];
        return;
    }
}

- (void)scanViewController:(UIViewController *)viewController didScanCardWithNumber:(NSString *)number type:(NSString *)metadataType
{
    
}

@end
