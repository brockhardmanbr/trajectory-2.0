//
//  TRJUserListTableViewCell.m
//  Trajectory
//
//  Created by Brock Hardman on 5/1/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import "TRJUserListTableViewCell.h"
#import "TRJUtilityCircleImageView.h"
#import "TRJParseUser.h"
#import <Parse/Parse.h>

@interface TRJUserListTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet TRJUtilityCircleImageView *circleImageView;
@property (nonatomic, strong) TRJParseUser *user;

@end

@implementation TRJUserListTableViewCell

- (void)configureWithUser:(TRJParseUser *)user
{
    self.user = user;
    
    [self.circleImageView updateImage];
    [self updateWithUser:user];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    [self cleanupCell];
}

- (CGSize)cellSize
{
    return CGSizeZero;
}

- (void)updateWithUser:(TRJParseUser *)user
{
    NSArray *firstLast = [user.username componentsSeparatedByString: @"."];
    NSString *first = [[[firstLast objectAtIndex:0] substringToIndex:1] uppercaseString];
    NSString *last = [[[firstLast objectAtIndex:1] substringToIndex:1] uppercaseString];
    [self configureImageViewWithString:[NSString stringWithFormat:@"%@%@", first, last]];
    
    self.nameLabel.text = user.name;
    self.titleLabel.text = user.position;
}

- (void)configureImageViewWithString:(NSString *)string
{
    UIView *initialsView = [[UIView alloc] initWithFrame:self.circleImageView.bounds];
    UILabel *initialsLabel = [[UILabel alloc] initWithFrame:initialsView.frame];
    initialsLabel.text = string;
    initialsLabel.textAlignment = NSTextAlignmentCenter;
    initialsLabel.textColor = [UIColor whiteColor];
    [initialsView addSubview:initialsLabel];
    [self.circleImageView addSubview:initialsView];
}

- (void)cleanupCell
{
    self.nameLabel.text = @"";
    self.titleLabel.text = @"";
    self.circleImageView.image = nil;
}

@end
