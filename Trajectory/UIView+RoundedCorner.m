//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "UIView+RoundedCorner.h"

@implementation UIView (RoundedCorner)

- (void)viewWithRoundedCorners
{
    self.layer.cornerRadius = 5;
    self.layer.masksToBounds = YES;
}

- (void)viewWithRoundedCornersWithRadius:(CGFloat)radius
{
    self.layer.cornerRadius = radius;
    self.layer.masksToBounds = YES;
}

@end
