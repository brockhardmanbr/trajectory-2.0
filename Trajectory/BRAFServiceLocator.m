//
//  BRAFServiceLocator.m
//  BRAFoundation
//
//  Created by Austen Green on 10/26/12.
//  Copyright (c) 2012 Bottle Rocket, LLC. All rights reserved.
//

#import "BRAFServiceLocator.h"

NSString * const BRAFServiceLocatorDefaultContext = @"BRAFServiceLocatorDefaultContext";

@interface BRAFServiceLocator ()

@property (nonatomic, strong) NSMutableDictionary *serviceMap;

@end

@implementation BRAFServiceLocator

#ifdef DEBUG
+ (void)load {
    [[NSUserDefaults standardUserDefaults] setValue:@"XCTestLog,BRACoverageObserver"
                                             forKey:@"XCTestObserverClass"];
}
#endif

+ (instancetype)sharedLocator
{
    static id sharedLocator;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedLocator = [[self alloc] init];
	});
	return sharedLocator;
}

- (id)init
{
	self = [super init];
	if (self)
	{
		_serviceMap = [[NSMutableDictionary alloc] init];
	}
	return self;
}

#pragma mark - Private methods

- (NSString *)keyFromProtocol:(Protocol *)protocol context:(NSString *)context
{
	NSString *protocolName = NSStringFromProtocol(protocol);
	   
    return [NSString stringWithFormat:@"%@.%@", protocolName, context];
}

- (id)resolvedObjectForKey:(NSString *)key withParameters:(NSArray *)parameters
{
	BRAFServiceLocatorReturnBlock block = [self.serviceMap objectForKey:key];
	id object = nil;
	
	NSAssert(block, @"No block for key: %@", key);
	if (block)
		object = block(parameters);
	
	return object;
}

#pragma mark - Service registration methods

- (void)registerServiceReturnBlock:(BRAFServiceLocatorReturnBlock)block protocol:(Protocol *)protocol
{
    [self registerServiceReturnBlock:block protocol:protocol context:BRAFServiceLocatorDefaultContext];
}

- (void)registerServiceReturnBlock:(BRAFServiceLocatorReturnBlock)block protocol:(Protocol *)protocol context:(NSString *)context
{
	NSAssert(block, @"Cannot register nil block for protocol: %@", NSStringFromProtocol(protocol));
	NSAssert(protocol, @"Cannot register block for nil protocol");
	NSAssert([context length] > 0, @"Cannot register block for nil context");
    
    NSString *key = [self keyFromProtocol:protocol context:context];
	[self.serviceMap setObject:[block copy] forKey:key];
}

#pragma mark - Object resolution methods

- (id)serviceForProtocol:(Protocol *)protocol
{
    return [self serviceForProtocol:protocol context:BRAFServiceLocatorDefaultContext parameters:nil];
}

- (id)serviceForProtocol:(Protocol *)protocol parameters:(NSArray *)parameters
{
    return [self serviceForProtocol:protocol context:BRAFServiceLocatorDefaultContext parameters:parameters];
}

- (id)serviceForProtocol:(Protocol *)protocol context:(NSString *)context
{
    return [self serviceForProtocol:protocol context:context parameters:nil];
}

- (id)serviceForProtocol:(Protocol *)protocol context:(NSString *)context parameters:(NSArray *)parameters
{
    NSAssert(protocol, @"Protocol must not be nil");
	NSAssert([context length] > 0, @"Cannot resolveObject for nil context");

	NSString *key = [self keyFromProtocol:protocol context:context];
	id object = [self resolvedObjectForKey:key withParameters:parameters];
    
	if (object)
		NSAssert([object conformsToProtocol:protocol], @"%@ does not conform to protocol: %@", object, key);
	
	return object;
}

@end
