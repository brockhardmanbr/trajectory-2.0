//
//  TRJProfile.h
//  Trajectory
//
//  Created by Brock Hardman on 4/26/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TRJProfileImage.h"

@interface TRJProfile : NSObject

@property (nonatomic, copy) NSString *_id;
@property (nonatomic, copy) NSString *emailAddress;
@property (nonatomic, copy) NSString *firstName;
@property (nonatomic, copy) NSString *lastName;
@property (nonatomic, copy) NSString *imageId;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;
- (NSDictionary *)toDictionary;

@end
