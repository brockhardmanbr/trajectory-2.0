//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIScrollView+KeyboardAdjusting.h"

@interface TRJUtilityKeyboardAdjustingCollectionView : UICollectionView <UITextFieldDelegate, UITextViewDelegate>

- (BOOL)focusNextTextField;
- (void)scrollToActiveTextField;

@end

