//
//  TRJScreen1ViewController.m
//  Trajectory
//
//  Created by Brock Hardman on 3/21/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import "TRJTimelineViewController.h"
#import "TRJParseManager.h"
#import "TRJPostOptionsView.h"

@interface TRJTimelineViewController() 

@property (nonatomic, strong) UIView *coverScreenView;
@property (nonatomic, strong) UIView *circleView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *postOptions;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@end

@implementation TRJTimelineViewController

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"timelineTableVC"])
    {
        TRJTimelineTableViewController *timelineTableVC = [segue destinationViewController];
        timelineTableVC.delegate = self;
        self.postOptions = [[TRJParseManager sharedManager] configurePostOptionsFromJSON:@"postOptions"];
    }
}

- (void)userDidLongPressTimelineWithLongPress:(UILongPressGestureRecognizer *)longPress fromTableView:(UITableView *)tableView
{
    self.tableView = tableView;
    CGPoint pressLocation = [longPress locationInView:longPress.view];
    
    if (longPress.state == UIGestureRecognizerStateBegan)
    {
        [self buildCircleWithPressLocation:pressLocation];
    }
    else if (longPress.state == UIGestureRecognizerStateEnded)
    {
        [self addTapGestureToCoverView];
    }
    else
    {
        [self closeCoverViews];
    }
}

- (void)buildCircleWithPressLocation:(CGPoint)location
{
    [self addCoverScreenForSelection];
    [self addPostToolToCoverScreen];
}

- (void)addTapGestureToCoverView
{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userDidTapCoverView:)];
    [self.coverScreenView addGestureRecognizer:tap];
}

- (void)addPostToolToCoverScreen
{
    TRJPostOptionsView *postOptionsView = (TRJPostOptionsView *)[[[NSBundle mainBundle] loadNibNamed:@"TRJPostOptionsView" owner:self options:nil] firstObject];
    [postOptionsView configureWithOptions:self.postOptions];
    postOptionsView.center = self.coverScreenView.center;
    [self.coverScreenView addSubview:postOptionsView];
}

- (void)addCoverScreenForSelection;
{
    UIVisualEffectView *coverView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleDark]];
    [coverView setAlpha:0];
    coverView.frame = self.view.frame;
    [self.view addSubview:coverView];
    self.coverScreenView = coverView;
    
    [UIView animateWithDuration:0.5 animations:^{
        [coverView setAlpha:1.0];
    }];
    
    [self.tableView setScrollEnabled:NO];
}

- (void)closeCoverViews
{
    [self removeCoverScreen];
    [self removeCircleView];
    [self updateNavigationLayout];
    
    [self.tableView setScrollEnabled:YES];
}

- (void)removeCoverScreen
{
    [UIView animateWithDuration:0.5 animations:^{
        [self.coverScreenView setAlpha:0.0];
    } completion:^(BOOL finished) {
        [self.coverScreenView removeFromSuperview];
        self.coverScreenView = nil;
    }];
}

- (void)removeCircleView
{
    [self.circleView removeFromSuperview];
    self.circleView = nil;
}

#pragma mark - Gesture Recognizers

- (void)userDidTapCoverView:(UITapGestureRecognizer *)tap
{
    [self closeCoverViews];
}

@end
