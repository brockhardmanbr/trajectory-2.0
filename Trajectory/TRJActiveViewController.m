//
//  TRJActiveViewController.m
//  Trajectory
//
//  Created by Brock Hardman on 4/5/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import "TRJActiveViewController.h"

@interface TRJActiveViewController ()

@property (nonatomic, readwrite) NSString *navigationTitle;
@property (nonatomic, readwrite) NSString *navigationImage;

@end

@implementation TRJActiveViewController

- (void)configureWithNavigationTitle:(NSString *)navigationTitle navigationImage:(NSString *)navigationImage
{
    self.navigationTitle = navigationTitle;
    self.navigationImage = navigationImage;
}

- (void)updateNavigationLayout
{
    [self.refreshDelegate layoutNeedsRefresh];
}

@end
