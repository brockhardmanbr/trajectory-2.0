//Patch out NSLog statements in release mode
#ifdef DEBUG
#define DebugLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#define DebugLog(s, ...)
#endif

#ifdef DEBUG
// outputs the specified code block (can be multi-line)
#define DebugCode(BLOCK) BLOCK
#else
#define DebugCode(BLOCK) ;
#endif
