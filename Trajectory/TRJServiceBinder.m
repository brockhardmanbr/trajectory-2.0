//
//  TRJServiceBinder.m
//  Trajectory
//
//  Created by Brock Hardman on 3/4/15.
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "TRJServiceBinder.h"
#import "TRJUtilityAlertViewCustom.h"
#import "TRJUtilityCamera.h"
#import "TRJURLConfigurationProvider.h"
#import "TRJPostsProvider.h"
#import "TRJProfileProvider.h"
#import "TRJDateFormatterProvider.h"
#import "TRJUtilityKeyboardAdjustingScrollView.h"
#import "TRJActiveControllersProvider.h"
#import "TRJLoginViewController.h"

static NSString * const kDefaultControllerPlist = @"ActiveControllers";

@implementation TRJServiceBinder

+ (void)bindServices
{
    [self bindCameraServices];
    [self bindAlertViewCustom];
//    [self bindTRJURLConfigurationProvider];
//    [self bindTRJPostsProvider];
//    [self bindProfileProvider];
    [self bindDateFormatterProvider];
    [self bindControllersWithNav];
    [self bindLoginController];
}

//Bind Camera Services
+ (void)bindCameraServices
{
    [self bindCameraWithCamera];
    [self bindCameraWithRoll]; 
    [self bindCameraWithMultiSelectRoll];
    [self bindCameraWithSavedPhotos];
    [self bindCameraWithBarcodeScanner];
}

+ (void)bindCameraWithCamera
{
    [[BRAFServiceLocator sharedLocator] registerServiceReturnBlock:^id(NSArray *parameters) {
        return [[TRJUtilityCamera alloc] initWithObject:parameters[0] withInitialState:TRJCameraStateStill withCameraType:TRJCameraTypeCamera allowsMultiSelect:NO withCompletionBlock:parameters[1]];
    } protocol:@protocol(TRJUserImageProvider) context:kTRJCameraWithCameraContext];
}

+ (void)bindCameraWithRoll
{
    [[BRAFServiceLocator sharedLocator] registerServiceReturnBlock:^id(NSArray *parameters) {
        return [[TRJUtilityCamera alloc] initWithObject:parameters[0] withInitialState:TRJCameraStateStill withCameraType:TRJCameraTypeRoll allowsMultiSelect:NO withCompletionBlock:parameters[1]];
    } protocol:@protocol(TRJUserImageProvider) context:kTRJCameraWithRollContext];
}

+ (void)bindCameraWithMultiSelectRoll
{
    [[BRAFServiceLocator sharedLocator] registerServiceReturnBlock:^id(NSArray *parameters) {
        return [[TRJUtilityCamera alloc] initWithObject:parameters[0] withInitialState:TRJCameraStateStill withCameraType:TRJCameraTypeRoll allowsMultiSelect:YES withCompletionBlock:parameters[1]];
    } protocol:@protocol(TRJUserImageProvider) context:kTRJCameraWithMultiSelectRollContext];
}

+ (void)bindCameraWithSavedPhotos
{
    [[BRAFServiceLocator sharedLocator] registerServiceReturnBlock:^id(NSArray *parameters) {
        return [[TRJUtilityCamera alloc] initWithObject:parameters[0] withInitialState:TRJCameraStateStill withCameraType:TRJCameraTypeSavedPhotos allowsMultiSelect:NO withCompletionBlock:parameters[1]];
    } protocol:@protocol(TRJUserImageProvider) context:kTRJCameraWithSavedPhotosContext];
}

+ (void)bindCameraWithBarcodeScanner
{
    [[BRAFServiceLocator sharedLocator] registerServiceReturnBlock:^id(NSArray *parameters) {
        return [[TRJUtilityCamera alloc] initWithObject:parameters[0] withInitialState:TRJCameraStateVideo withCameraType:TRJCameraTypeBarcodeScanner allowsMultiSelect:NO withCompletionBlock:parameters[1]];
    } protocol:@protocol(TRJUserVideoProvider) context:kTRJCameraWithBarcodeScannerContext];
}

//Bind Alert Views
+ (void)bindAlertViewCustom
{
    [[BRAFServiceLocator sharedLocator] registerServiceReturnBlock:^id(NSArray *parameters) {
        return [[TRJUtilityAlertViewCustom alloc] initWithObject:parameters[0] withCustomController:parameters[1] withCompletionBlock:parameters[2]];
    } protocol:@protocol(TRJAlertViewProvider) context:kAlertViewCustomContext];
}

//Bind Controllers
+ (void)bindControllersWithNav
{
    [[BRAFServiceLocator sharedLocator] registerServiceReturnBlock:^id(NSArray *parameters) {
        return [[TRJActiveControllersProvider alloc] initWithJsonURL:parameters[0] serverType:TRJActiveControllersProviderServerTypeParse withCompletionBlock:parameters[1]];
    } protocol:@protocol(TRJActiveControllersProviderProtocol) context:kActiveControllersContext];
}

//Bind Login Controller
+ (void)bindLoginController
{
    [[BRAFServiceLocator sharedLocator] registerServiceReturnBlock:^id(NSArray *parameters) {
        NSInteger serviceEnumValue;
        if ([parameters[0] isKindOfClass:[NSNumber class]])
        {
            serviceEnumValue = [(NSNumber *)parameters[0] integerValue];
        }
        
        TRJLoginViewController *loginViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:[TRJLoginViewController storyboardIdentifier]];
        [loginViewController configureWithService:serviceEnumValue withCompletionBlock:parameters[1]];
        return loginViewController;
        
    } protocol:@protocol(TRJLoginControllerProtocol) context:kLoginControllerContext];
}

+ (void)bindDateFormatterProvider
{
    [[BRAFServiceLocator sharedLocator] registerServiceReturnBlock:^id(NSArray *parameters) {
        static TRJDateFormatterProvider *provider = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            provider = [[TRJDateFormatterProvider alloc] init];
        });
        return provider;
    } protocol:@protocol(TRJDateFormatterProviderProtocol)];
}

//+ (void)bindTRJURLConfigurationProvider
//{
//    [[BRAFServiceLocator sharedLocator] registerServiceReturnBlock:^id(NSArray *parameters) {
//        // needs to be static because it saves loaded config info
//        static TRJURLConfigurationProvider *provider = nil;
//        static dispatch_once_t onceToken;
//        dispatch_once(&onceToken, ^{
//            provider = [[TRJURLConfigurationProvider alloc] initWithBaseURL:[NSURL URLWithString:@""]];
//        });
//        return provider;
//    } protocol:@protocol(TRJURLConfigurationProtocol)];
//}
//
//+ (void)bindTRJPostsProvider
//{
//    [[BRAFServiceLocator sharedLocator] registerServiceReturnBlock:^id(NSArray *parameters) {
//        // needs to be static to save in-flight requests
//        static TRJPostsProvider *postsProvider = nil;
//        static dispatch_once_t onceToken;
//        dispatch_once(&onceToken, ^{
//            postsProvider = [[TRJPostsProvider alloc] initWithConfiguredBaseURL];
//        });
//        return postsProvider;
//    } protocol:@protocol(TRJPostsProviderProtocol)];
//}
//
//+ (void)bindProfileProvider
//{
//    [[BRAFServiceLocator sharedLocator] registerServiceReturnBlock:^id(NSArray *parameters) {
//        static TRJProfileProvider *provider = nil;
//        static dispatch_once_t onceToken;
//        dispatch_once(&onceToken, ^{
//            provider = [[TRJProfileProvider alloc] initWithConfiguredBaseURL];
//        });
//        return provider;
//    } protocol:@protocol(TRJProfileProtocol)];
//}

@end
