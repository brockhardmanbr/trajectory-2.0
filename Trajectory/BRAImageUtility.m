//
//  BRAImageUtility.m
//  CCFS
//
//  Created by Brian Drell on 4/11/14.
//  Copyright (c) 2014 Bottle Rocket, LLC. All rights reserved.
//

#import "BRAImageUtility.h"
#import "UIImage+ImageEffects.h"
@import CoreImage;

// TODO: Refactor this to use a real networking library with real image caching and stuff like that.

@implementation BRAImageUtility

+ (void)applyExtraLightEffectToImage:(UIImage *)image completion:(BRAImageUtilityCompletionBlock)block
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage *blurredImage = [image applyExtraLightEffect];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (block) block(blurredImage);
        });
    });
}

+ (void)applyExtraLightEffectToView:(UIView *)view inRect:(CGRect)rect completion:(BRAImageUtilityCompletionBlock)block
{
    UIGraphicsBeginImageContext(view.bounds.size);
    [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:YES];
    UIImage *snapshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    snapshot = [self croppedImage:snapshot inRect:rect];
    [self applyExtraLightEffectToImage:snapshot completion:block];
}

+ (void)applyExtraLightEffectToKeyWindowWithCompletion:(BRAImageUtilityCompletionBlock)block
{
    [self applyExtraLightEffectToView:[[UIApplication sharedApplication] keyWindow] inRect:[[[UIApplication sharedApplication] keyWindow] bounds] completion:block];
}

+ (void)applyExtraLightEffectToImageWithURL:(NSURL *)url completion:(BRAImageUtilityCompletionBlock)block
{
    [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:url] queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (!connectionError && data)
        {
            UIImage *image = [UIImage imageWithData:data];
            [self applyExtraLightEffectToImage:image completion:block];
        }
        else if (block)
        {
            block(nil);
        }
    }];
}

+ (void)applyLightEffectToImage:(UIImage *)image completion:(BRAImageUtilityCompletionBlock)block
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage *blurredImage = [image applyLightEffect];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (block) block(blurredImage);
        });
    });
}

+ (void)applyLightEffectToView:(UIView *)view inRect:(CGRect)rect completion:(BRAImageUtilityCompletionBlock)block
{
    UIGraphicsBeginImageContext(view.bounds.size);
    [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:YES];
    UIImage *snapshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    snapshot = [self croppedImage:snapshot inRect:rect];
    [self applyLightEffectToImage:snapshot completion:block];
}

+ (void)applyLightEffectToKeyWindowWithCompletion:(BRAImageUtilityCompletionBlock)block
{
    [self applyLightEffectToView:[[UIApplication sharedApplication] keyWindow] inRect:[[[UIApplication sharedApplication] keyWindow] bounds] completion:block];
}

+ (void)applyLightEffectToImageWithURL:(NSURL *)url completion:(BRAImageUtilityCompletionBlock)block
{
    [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:url] queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (!connectionError && data)
        {
            UIImage *image = [UIImage imageWithData:data];
            [self applyLightEffectToImage:image completion:block];
        }
        else if (block)
        {
            block(nil);
        }
    }];
}

+ (void)applyDarkEffectToImage:(UIImage *)image completion:(BRAImageUtilityCompletionBlock)block
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage *blurredImage = [image applyDarkEffect];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (block) block(blurredImage);
        });
    });
}

+ (void)applyDarkEffectToView:(UIView *)view inRect:(CGRect)rect completion:(BRAImageUtilityCompletionBlock)block
{
    UIGraphicsBeginImageContext(view.bounds.size);
    [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:YES];
    UIImage *snapshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    snapshot = [self croppedImage:snapshot inRect:rect];
    [self applyDarkEffectToImage:snapshot completion:block];
}

+ (void)applyDarkEffectToKeyWindowWithCompletion:(BRAImageUtilityCompletionBlock)block
{
    [self applyDarkEffectToView:[[UIApplication sharedApplication] keyWindow] inRect:[[[UIApplication sharedApplication] keyWindow] bounds] completion:block];
}

+ (void)applyDarkEffectToImageWithURL:(NSURL *)url completion:(BRAImageUtilityCompletionBlock)block
{
    [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:url] queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (!connectionError && data)
        {
            UIImage *image = [UIImage imageWithData:data];
            [self applyDarkEffectToImage:image completion:block];
        }
        else if (block)
        {
            block(nil);
        }
    }];
}

+ (UIImage *)croppedImage:(UIImage *)image inRect:(CGRect)cropRect
{
//    CGRect fullImageRect = CGRectMake(0, 0, image.size.width, image.size.height);
//    cropRect = CGRectIntersection(fullImageRect, cropRect);
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    UIImage *croppedImage = [UIImage imageWithCGImage:imageRef scale:image.scale orientation:image.imageOrientation];
    CFRelease(imageRef);
    return croppedImage;
}

+ (UIImage *)renderedView:(UIView *)view
{
    UIGraphicsBeginImageContext(view.bounds.size);
    [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:YES];
    UIImage *snapshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return snapshot;
}

+ (UIImage *)renderedView:(UIView *)view croppedToRect:(CGRect)cropRect
{
    UIImage *render = [self renderedView:view];
    render = [self croppedImage:render inRect:cropRect];
    return render;
}

// Core Image. Slow. Use sparingly, and on a background thread.
+ (UIImage *)image:(UIImage *)image WithSaturation:(CGFloat)saturation
{
    NSAssert(saturation >= 0 && saturation <= 1, @"Saturation must be 0 to 1");
    CIImage *ciImage = [CIImage imageWithCGImage:[image CGImage]];
    CIFilter *satFilter = [CIFilter filterWithName:@"CIColorControls"];
    [satFilter setValue:ciImage forKey:@"inputImage"];
    [satFilter setValue:@0 forKey:@"inputSaturation"];
    
    CIImage *outputImage = satFilter.outputImage;
    
    CIContext *context = [CIContext contextWithOptions:nil];
    
    UIImage *desaturatedImage = [UIImage imageWithCGImage:[context createCGImage:outputImage fromRect:outputImage.extent]];
    return desaturatedImage;
}

@end
