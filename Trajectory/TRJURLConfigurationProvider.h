//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "TRJURLConfigurationProtocol.h"

@interface TRJURLConfigurationProvider : OVCHTTPSessionManager <TRJURLConfigurationProtocol>

- (instancetype)initWithDefaultURL NS_DESIGNATED_INITIALIZER;

@end
