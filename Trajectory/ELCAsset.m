//
//  Asset.m
//
//  Created by ELC on 2/15/11.
//  Copyright 2011 ELC Technologies. All rights reserved.
//

#import "ELCAsset.h"
#import "ELCAssetTablePicker.h"

@implementation ELCAsset

//Using auto synthesizers
- (NSString *)description
{
    return [NSString stringWithFormat:@"ELCAsset index:%ld",(long)self.index];
}

- (id)initWithAsset:(ALAsset*)asset
{
	self = [super init];
	if (self) {
		self.asset = asset;
        _selected = NO;
    }
	return self;	
}

- (void)toggleSelection
{
    self.selected = !self.selected;
}

- (void)setSelected:(BOOL)selected
{
    id <ELCAssetDelegate> parent = _parent;
    
    if (selected) {
        if ([parent respondsToSelector:@selector(shouldSelectAsset:)]) {
            if (![parent shouldSelectAsset:self]) {
                return;
            }
        }
    } else {
        if ([parent respondsToSelector:@selector(shouldDeselectAsset:)]) {
            if (![parent shouldDeselectAsset:self]) {
                return;
            }
        }
    }
    _selected = selected;
    if (selected) {
        if (parent != nil && [parent respondsToSelector:@selector(assetSelected:)]) {
            [parent assetSelected:self];
        }
    } else {
        if (parent != nil && [parent respondsToSelector:@selector(assetDeselected:)]) {
            [parent assetDeselected:self];
        }
    }
}

- (NSComparisonResult)compareWithIndex:(ELCAsset *)_ass
{
    if (self.index > _ass.index) {
        return NSOrderedDescending;
    }
    else if (self.index < _ass.index)
    {
        return NSOrderedAscending;
    }
    return NSOrderedSame;
}

@end

