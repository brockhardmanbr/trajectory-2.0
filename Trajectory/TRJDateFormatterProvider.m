//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "TRJDateFormatterProvider.h"

@interface TRJDateFormatterProvider()

@property (nonatomic, strong) NSMutableDictionary *dateFormatterDictionary;
@property (nonatomic, strong) NSOperationQueue *operationQueue;

@end

@implementation TRJDateFormatterProvider

- (instancetype)init
{
	self = [super init];
	if (self)
	{
		_dateFormatterDictionary = [[NSMutableDictionary alloc] init];
		_operationQueue = [[NSOperationQueue alloc] init];
		_operationQueue.maxConcurrentOperationCount = 1;
	}
	return self;
}

- (NSDateFormatter *)dateFormatterForFormatString:(NSString *)formatString
{
	// THIS MUST BE IN A QUEUE because it could come from multiple threads at the same time.
	// Normally, one might use a separate date formatter per class, and then dispatch_once
	// in each class. That's fine, but since we have so many date formatters, let's
	// create fewer of them.
	// if there's already a formatter, return the existing one.
	__block NSDateFormatter *returnFormatter = nil;
	NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
		if (!self.dateFormatterDictionary[formatString])
		{
			NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
			formatter.dateFormat = formatString;
			formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
			self.dateFormatterDictionary[formatString] = formatter;
		}
		returnFormatter = self.dateFormatterDictionary[formatString];
	}];

	[self.operationQueue addOperations:@[operation] waitUntilFinished:YES];
	
	return returnFormatter;
}

- (NSDateFormatter *)dateFormatterForStyle:(NSDateFormatterStyle)style
{
	// THIS MUST BE IN A QUEUE because it could come from multiple threads at the same time.
	// Normally, one might use a separate date formatter per class, and then dispatch_once
	// in each class. That's fine, but since we have so many date formatters, let's
	// create fewer of them.
	// if there's already a formatter, return the existing one.
	NSString *styleString = [NSString stringWithFormat:@"style: %lu",style];
	if (!self.dateFormatterDictionary[styleString])
	{
		NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
			NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
			formatter.dateStyle = style;
			formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
			self.dateFormatterDictionary[styleString] = formatter;
		}];
		
		[self.operationQueue addOperations:@[operation] waitUntilFinished:YES];
	}
	
	return self.dateFormatterDictionary[styleString];
}

@end
