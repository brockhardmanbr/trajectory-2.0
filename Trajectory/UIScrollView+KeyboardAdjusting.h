//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (KeyboardAdjusting)

- (BOOL)KeyboardAdjusting_focusNextTextField;
- (void)KeyboardAdjusting_scrollToActiveTextField;
- (void)KeyboardAdjusting_keyboardWillShow:(NSNotification *)notification;
- (void)KeyboardAdjusting_keyboardWillHide:(NSNotification *)notification;
- (void)KeyboardAdjusting_updateContentInset;
- (void)KeyboardAdjusting_updateFromContentSizeChange;
- (void)KeyboardAdjusting_assignTextDelegateForViewsBeneathView:(UIView *)view;
- (UIView *)KeyboardAdjusting_findFirstResponderBeneathView:(UIView *)view;
- (CGSize)KeyboardAdjusting_calculatedContentSizeFromSubviewFrames;

@end
