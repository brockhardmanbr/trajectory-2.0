//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "TRJUtilityBarcodeScannerViewController.h"
#import "TRJUtilityAlertViewCustom.h"
@import AVFoundation;

@interface TRJUtilityBarcodeScannerViewController () <AVCaptureMetadataOutputObjectsDelegate>

@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureDevice *captureDevice;
@property (nonatomic, strong) AVCaptureDeviceInput *captureInput;
@property (nonatomic, strong) AVCaptureMetadataOutput *captureOutput;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *previewLayer;
@property (nonatomic, strong) AVCaptureVideoDataOutput *videoOutput;

@property (weak, nonatomic) IBOutlet UIView *avContainerView;
@property (weak, nonatomic) IBOutlet UIView *barCodeHighlightView;

@end

@implementation TRJUtilityBarcodeScannerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupScanner];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    [self.captureSession startRunning];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
	__strong __typeof(self.avContainerView) avContainerView = self.avContainerView;
    self.previewLayer.frame = avContainerView.bounds;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self.captureSession stopRunning];
}

- (void)setupScanner
{
    self.captureSession = [[AVCaptureSession alloc] init];
    UIView *barCodeView = self.barCodeHighlightView;
    barCodeView.layer.borderColor = [[UIColor redColor] CGColor];
    barCodeView.layer.borderWidth = 2;
    self.captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    NSError *lockError;
    [self.captureDevice lockForConfiguration:&lockError];
    if (!lockError)
    {
        [self.captureDevice setTorchMode:AVCaptureTorchModeAuto];
    }
    [self.captureDevice unlockForConfiguration];
    
    NSError *captureError;
    self.captureInput = [AVCaptureDeviceInput deviceInputWithDevice:self.captureDevice error:&captureError];
    if (self.captureInput && !captureError)
    {
        [self.captureSession addInput:self.captureInput];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Capture Unavailable" message:@"This device's camera is unavailable for bar code capture." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        
        __strong __typeof(self.delegate) delegate = self.delegate;
        [delegate scanViewController:self didReturnErrorWithObject:alertView];
        
        return;
    }
    
    self.captureOutput = [[AVCaptureMetadataOutput alloc] init];
    [self.captureOutput setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    [self.captureSession addOutput:self.captureOutput];
    
    self.videoOutput = [[AVCaptureVideoDataOutput alloc] init];
    [self.captureSession addOutput:self.videoOutput];
    self.captureOutput.metadataObjectTypes = @[AVMetadataObjectTypeUPCECode,
                                               AVMetadataObjectTypeCode39Code,
                                               AVMetadataObjectTypeCode39Mod43Code,
                                               AVMetadataObjectTypeEAN13Code,
                                               AVMetadataObjectTypeEAN8Code,
                                               AVMetadataObjectTypeCode93Code,
                                               AVMetadataObjectTypeCode128Code,
                                               AVMetadataObjectTypePDF417Code];
    
    UIView *avContainerView = self.avContainerView;
    CGRect bounds = avContainerView.bounds;
    AVCaptureVideoPreviewLayer *previewLayer = self.previewLayer;
    previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:self.captureSession];
    previewLayer.frame = bounds;
    previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [avContainerView.layer insertSublayer:previewLayer atIndex:0];
}


#pragma mark - IBAction

- (IBAction)cancelTapped:(UIBarButtonItem *)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Capture delegate

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    for (AVMetadataObject *metadata in metadataObjects)
    {
        NSString *numberString = [(AVMetadataMachineReadableCodeObject *)metadata stringValue];
        if (numberString.length)
        {
            if ([metadata.type isEqualToString:AVMetadataObjectTypeEAN13Code]){
                if ([numberString hasPrefix:@"0"] && [numberString length] > 1)
                    numberString = [numberString substringFromIndex:1];
            }
        }
        
        [self.captureSession removeOutput:captureOutput];
        
        UIView *highlightView = self.barCodeHighlightView;
        UILabel *codeLabel = [[UILabel alloc] initWithFrame:highlightView.frame];
        codeLabel.center = highlightView.center;
        codeLabel.text = numberString;
        codeLabel.alpha = 0;
        [UIView animateWithDuration:0.25 animations:^{
            UIView *barCodeView = self.barCodeHighlightView;
            barCodeView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.8];
            codeLabel.alpha = 1;
        }];
        
        id<TRJCameraBarcodeScannerViewControllerDelegate> delegate = self.delegate;
        [delegate scanViewController:self didScanCardWithNumber:numberString type:metadata.type];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2. * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self dismissViewControllerAnimated:YES completion:nil];
        });
    }
}

@end
