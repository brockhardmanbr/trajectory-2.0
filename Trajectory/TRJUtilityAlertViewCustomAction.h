//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

@class TRJUtilityAlertViewCustomAction;

typedef NS_ENUM(NSInteger, AlertActionStyleCustom) {
    AlertActionStyleCustomDefault = 0,
    AlertActionStyleCustomCancel,
    AlertActionStyleCustomDestructive
};

typedef void(^PSUtilityAlertViewCustomActionHandler)(TRJUtilityAlertViewCustomAction *action);

@interface TRJUtilityAlertViewCustomAction : NSObject

+ (instancetype)actionWithTitle:(NSString *)title style:(AlertActionStyleCustom)style handler:(void (^)(TRJUtilityAlertViewCustomAction *action))handler;

@property (nonatomic, readonly) NSString *actionTitle;
@property (nonatomic, readonly) AlertActionStyleCustom actionStyle;
@property (nonatomic, readonly) PSUtilityAlertViewCustomActionHandler actionHandler;

@end

