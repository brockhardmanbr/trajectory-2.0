//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "UIView+CircularMask.h"

@implementation UIView (CircularMask)

- (void)addCircleMaskWithBorderColor:(UIColor *)borderColor borderWidth:(CGFloat)borderWidth fillColor:(UIColor *)fillColor
{
    [self createViewMaskLayer];
    [self createViewMaskBorderLayerWithColor:borderColor width:borderWidth fillColor:fillColor];
}

- (void)createViewMaskLayer
{
    CAShapeLayer *mask = [CAShapeLayer layer];
    self.layer.mask = mask;
    mask.frame = self.bounds;
    
    CGPathRef path = CGPathCreateWithEllipseInRect([self rectForMask], 0);
    mask.path = path;
    CFRelease(path);
}

- (void)createViewMaskBorderLayerWithColor:(UIColor *)color width:(CGFloat)width fillColor:(UIColor *)fillColor
{
    CAShapeLayer *mask = [CAShapeLayer layer];
    mask.frame = self.bounds;
    mask.strokeColor = [color CGColor];
    mask.lineWidth = width;
    mask.fillColor = [fillColor CGColor];
    
    [self.layer addSublayer:mask];
    
    CGRect borderBox = CGRectInset([self rectForMask], 0, 0);
    CGPathRef borderPath = CGPathCreateWithEllipseInRect(borderBox, 0);
    mask.path = borderPath;
    CFRelease(borderPath);
}

- (CGRect)rectForMask
{
    CGFloat viewDimension = fmin(CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds));
    CGFloat viewOriginX = CGRectGetMidX(self.bounds) - viewDimension / 2.;
    CGFloat viewOriginY = CGRectGetMidY(self.bounds) - viewDimension / 2.;
    CGRect maskBox = CGRectMake(viewOriginX, viewOriginY, viewDimension, viewDimension);
    return CGRectIntegral(maskBox);
}

@end
