//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "TRJUtilityAlertViewCustom.h"

@interface TRJUtilityAlertViewCustom()

@property (nonatomic, strong) id passedObject;
@property (nonatomic, strong) id customController;
@property (nonatomic, copy) TRJAlertViewCompletionBlock completionBlock;
@property (nonatomic, strong) UIViewController *presentingViewController;
@property (nonatomic, strong) UIViewController *presentedViewController;

@end

@implementation TRJUtilityAlertViewCustom

- (instancetype)initWithObject:(id)passedObject withCustomController:(id)customController withCompletionBlock:(TRJAlertViewCompletionBlock)completionBlock
{
    self = [super init];
    if (self)
    {
        _passedObject = passedObject;
        _completionBlock = completionBlock;
        _customController = customController;
        
        [self configureWithPassedObject:passedObject];
        [self configureWithCustomController:customController];
    }
    return self;
}

- (void)configureWithPassedObject:(id)passedObject
{
    if ([passedObject isKindOfClass:[UIViewController class]])
    {
        self.presentingViewController = (UIViewController *)passedObject;
    }
}

- (void)configureWithCustomController:(id)customController
{
    if ([customController isKindOfClass:[TRJUtilityAlertViewController class]])
    {
        self.presentedViewController = (TRJUtilityAlertViewController *)customController;
    }
}

- (void)showCustomAlert
{
    NSAssert(self.presentedViewController && self.presentingViewController, @"You must have a presenting and a presented view controller here.");
    
    [self.presentingViewController presentViewController:self.presentedViewController animated:YES completion:self.completionBlock];
}

@end
