//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

@interface TRJPostsConfiguration : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy) NSString *baseURLString;
@property (nonatomic, copy) NSString *allPostsEndpoint;
@property (nonatomic, copy) NSString *singlePostEndpoint;
@property (nonatomic, copy) NSString *createPostEndpoint;
@property (nonatomic, copy) NSString *deletePostEndpoint;
@property (nonatomic, copy) NSString *contentEndpoint;
@property (nonatomic, copy) NSString *metadataEndpoint;

@end
