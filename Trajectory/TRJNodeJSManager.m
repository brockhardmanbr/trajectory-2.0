//
//  TRJNodeJSManager.m
//  Trajectory
//
//  Created by Brock Hardman on 4/29/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import "TRJNodeJSManager.h"

@interface TRJNodeJSManager()

@property (nonatomic, strong) NSURLSession *session;

@end

@implementation TRJNodeJSManager

+ (instancetype)sharedManager
{
    static TRJNodeJSManager *_sharedManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[self alloc] init];
    });
    return _sharedManager;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self createSession];
    }
    return self;
}

- (void)createSession
{
    NSURLSession *session = [NSURLSession sharedSession];
    _session = session;
}

- (NSArray *)configureWithJsonURLInternal
{
    NSString *filePath =[[NSBundle mainBundle] pathForResource:@"activeViewControllers" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    return [self getControllersFromDictionary:dictionary];
}

- (NSArray *)configureWithJsonURLExternal:(NSString *)url
{
//    NSURL *jsonURL = [[NSURL alloc] initWithString:url];
//    
//    [[self.session dataTaskWithURL:jsonURL completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//        if (error)
//        {
//            NSLog(@"Error: %@", error.localizedDescription);
//            return @[];
//        }
//        else
//        {
//            NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
//            return [self getControllersFromDictionary:dictionary];
//        }
//    }] resume];
    
    return @[];
}

- (NSArray *)getControllersFromDictionary:(NSDictionary *)dictionary
{
    if (dictionary)
    {
        NSMutableArray *controllers = [[NSMutableArray alloc] init];
        
        NSArray *keys = [dictionary allKeys];
        NSArray *sortedKeys = [keys sortedArrayUsingComparator:^NSComparisonResult(NSString *key1, NSString *key2) {
            return [key1 compare:key2];
        }];
        
        for (NSUInteger i = 0; i < [sortedKeys count]; i++)
        {
            [controllers addObject:[dictionary objectForKey:[sortedKeys objectAtIndex:i]]];
        }
        
        return [NSArray arrayWithArray:controllers];
    }
    
    return @[];
}

@end
