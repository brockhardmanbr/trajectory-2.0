//
//  TRJScreen3ViewController.m
//  Trajectory
//
//  Created by Brock Hardman on 3/21/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import "TRJProfileViewController.h"
#import "TRJProfile.h"
#import "TRJProfileImage.h"

@interface TRJProfileViewController ()

@property (nonatomic, strong) TRJProfileImage *profileImage;
@property (nonatomic, strong) TRJProfile *profile;

@end

@implementation TRJProfileViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    [self createDummyProfile];
//    [self loadProfile];
}

- (void)createDummyProfile
{
    NSDictionary *dummyDict = @{
                                @"_id" : @"5541b9deaec1dbed11ed6414",
                                @"emailAddress" : @"brock.hardman111@bottlerocketstudios.com",
                                @"firstName" : @"Brock111",
                                @"lastName" : @"Hardman111",
                                @"imageId" : @"5541c961aec1dbed11ed641b"
                                };
    
    TRJProfile *dummyProfile = [[TRJProfile alloc] initWithDictionary:dummyDict];
    
//    TRJProfileImage *dummyProfileImage = [[TRJProfileImage alloc] init];
//    dummyProfileImage.image = [UIImage imageNamed:@"captain"];
    
//    [self saveProfileImage:dummyProfileImage];
    [self saveProfile:dummyProfile];
}

- (void)loadProfile
{
    NSString *pathSring = [[kNodeJSServerBaseURL stringByAppendingPathComponent:kNodeJSProfileEndpoint] stringByAppendingPathComponent:@"5541b9deaec1dbed11ed6414"];
    NSURL *url = [NSURL URLWithString:pathSring];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"GET";
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error == nil) {
            NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
            [self parseProfile:responseDictionary];
        }
    }];
    
    [dataTask resume];
}

- (void)saveProfile:(TRJProfile *)profile
{
    if (!profile)
    {
        return; //input safety check
    }
    
    NSString *pathString = [kNodeJSServerBaseURL stringByAppendingPathComponent:kNodeJSProfileEndpoint];
    
    BOOL isExistingLocation = profile._id;
    NSURL *url = isExistingLocation ? [NSURL URLWithString:[pathString stringByAppendingPathComponent:profile._id]] :
    [NSURL URLWithString:pathString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = isExistingLocation ? @"POST" : @"PUT";
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:[profile toDictionary] options:0 error:NULL];
    request.HTTPBody = data;
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error) {
            if ([request.HTTPMethod isEqualToString:@"PUT"])
            {
                NSLog(@"Successfully Added Profile : %@", profile);
            }
            else if ([request.HTTPMethod isEqualToString:@"POST"])
            {
                NSLog(@"Successfully Updated Profile : %@", profile);
            }
        }
    }];
    
    [dataTask resume];
}

- (void)saveProfileImage:(TRJProfileImage *)image
{
    NSURL* url = [NSURL URLWithString:[kNodeJSServerBaseURL stringByAppendingPathComponent:kNodeJSFilesEndpoint]];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    [request addValue:@"image/png" forHTTPHeaderField:@"Content-Type"];
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession* session = [NSURLSession sessionWithConfiguration:config];
    
    NSData* bytes = UIImagePNGRepresentation(image.image);
    NSURLSessionUploadTask* task = [session uploadTaskWithRequest:request fromData:bytes completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error == nil && [(NSHTTPURLResponse*)response statusCode] < 300) {
            NSDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
            image.imageId = responseDict[@"_id"];
            [self persistImage:image];
        }
    }];
    [task resume];
}

- (void)loadProfileImage:(NSString *)profileImageId
{
    NSURL* url = [NSURL URLWithString:[[kNodeJSServerBaseURL stringByAppendingPathComponent:kNodeJSFilesEndpoint] stringByAppendingPathComponent:profileImageId]];
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession* session = [NSURLSession sessionWithConfiguration:config];
    
    NSURLSessionDownloadTask* task = [session downloadTaskWithURL:url completionHandler:^(NSURL *fileLocation, NSURLResponse *response, NSError *error) {
        if (!error) {
            NSData* imageData = [NSData dataWithContentsOfURL:fileLocation];
            UIImage* image = [UIImage imageWithData:imageData];
            if (!image) {
                NSLog(@"unable to build image");
            }
            
            self.profileImage.image = image;
        }
    }];
    
    [task resume];
}

- (void)parseProfile:(NSDictionary *)profileDictionary
{
    TRJProfile *profile = [[TRJProfile alloc] initWithDictionary:profileDictionary];
    
    if (profile.imageId)
    {
        [self loadProfileImage:profile.imageId];
    }
    
    NSLog(@"Profile Data: %@", profile);
}

- (void)persistImage:(TRJProfileImage *)image
{
    if (image.image != nil && image.imageId == nil)
    {
        [self saveProfileImage:image];
        return;
    }
}

@end
