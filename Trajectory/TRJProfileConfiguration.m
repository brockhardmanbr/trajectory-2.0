//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "TRJProfileConfiguration.h"

@implementation TRJProfileConfiguration

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
	return @{
			 @"baseURLString" : @"base_url"
			 };
}

@end
