//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "TRJUtilityAlertViewCustomAction.h"

typedef NS_ENUM(NSInteger, PSUtilityAlertControllerStyleCustom) {
    PSUtilityAlertControllerStyleCustomActionSheet = 0,
    PSUtilityAlertControllerStyleCustomAlert
};

@interface TRJUtilityAlertViewController : UIViewController

+ (instancetype)alertViewControllerWithTitle:(NSString *)title message:(NSString *)message preferredStyle:(PSUtilityAlertControllerStyleCustom)preferredStyle backgroundColor:(UIColor *)backgroundColor;

- (void)addAction:(TRJUtilityAlertViewCustomAction *)action;
- (void)placeActionButtons;
@property (nonatomic, readonly) NSArray *actions;
- (void)addTextFieldWithConfigurationHandler:(void (^)(UITextField *textField))configurationHandler;
@property (nonatomic, readonly) NSArray *textFields;

@property (nonatomic, copy) NSString *titleMessage;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, strong) UIColor *backgroundColor;

@property (nonatomic, readonly) PSUtilityAlertControllerStyleCustom preferredStyle;

@end

