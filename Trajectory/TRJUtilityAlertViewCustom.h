//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "TRJUtilityAlertViewController.h"
#import "TRJAlertViewProvider.h"

typedef void(^TRJAlertViewCompletionBlock)(void);

@interface TRJUtilityAlertViewCustom : NSObject<TRJAlertViewProvider>

- (instancetype)initWithObject:(id)passedObject withCustomController:(id)customController withCompletionBlock:(TRJAlertViewCompletionBlock)completionBlock;

@end
