//
//  TRJActiveViewController.h
//  Trajectory
//
//  Created by Brock Hardman on 4/5/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import "TRJActiveViewControllerProtocol.h"
#import "TRJGlobalNavRefreshProtocol.h"

@protocol TRJActiveViewControllerRefreshProtocol <NSObject>

- (void)layoutNeedsRefresh;

@end

@interface TRJActiveViewController : UIViewController <TRJActiveViewControllerProtocol, TRJGlobalNavRefreshProtocol>

@property (nonatomic, readonly) NSString *navigationTitle;
@property (nonatomic, readonly) NSString *navigationImage;
@property (nonatomic) id<TRJActiveViewControllerRefreshProtocol> refreshDelegate;

@end
