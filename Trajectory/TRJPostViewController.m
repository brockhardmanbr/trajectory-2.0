//
//  TRJScreen2ViewController.m
//  Trajectory
//
//  Created by Brock Hardman on 3/21/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import "TRJPostViewController.h"
#import <Parse/Parse.h>

@interface TRJPostViewController ()

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *postButton;
@property (nonatomic, weak) id keyboardShownListener;
@property (nonatomic, weak) id keyboardHideListener;

@end

@implementation TRJPostViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self addBorderToView:self.textView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
    [self.view addGestureRecognizer:tap];
    
    self.postButton.layer.cornerRadius = 4;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:_keyboardShownListener];
    [[NSNotificationCenter defaultCenter] removeObserver:_keyboardHideListener];
}

- (void)addBorderToView:(UIView *)view
{
    CALayer *imageLayer = view.layer;
    [imageLayer setCornerRadius:10];
    [imageLayer setBorderWidth:1];
    imageLayer.borderColor=[[UIColor darkGrayColor] CGColor];
}

- (IBAction)postButtonTapped:(UIButton *)sender
{
    [self postData];
}

- (void)viewTapped:(UITapGestureRecognizer *)sender
{
    [self.view endEditing:YES];
}

- (void)postData
{
    // Create PFObject with recipe information
    PFObject *post = [PFObject objectWithClassName:@"Posts"];
    [post setObject:self.textView.text forKey:@"postText"];
    [post setObject:[PFUser currentUser].objectId forKey:@"userId"];
    [post setObject:[[PFUser currentUser] objectForKey:@"name"] forKey:@"name"];
    
    __weak __typeof(self)weakSelf = self;
    [post saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        
        if (!error)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Post Successful" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            weakSelf.textView.text = @"";
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kUserDidPostMessageNotification object:nil];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Post Upload Failure" message:[error localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        
    }];
}

//- (void)setupKeyboardBehavior
//{
//    __weak __typeof__(self) weakSelf = self;
//    self.keyboardShownListener = [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillShowNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
//        NSDictionary *userInfo = [note userInfo];
//        CGRect keyboardFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
//        
//    }];
//    
//    self.keyboardHideListener = [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillHideNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
//        
//    }];
//}

@end
