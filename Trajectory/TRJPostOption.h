//
//  TRJPostOption.h
//  Trajectory
//
//  Created by Brock Hardman on 5/3/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TRJPostOption : NSObject

@property (nonatomic, copy) NSString *postType;
@property (nonatomic, copy) NSString *postTitle;
@property (nonatomic, copy) NSString *postImage;

@end
