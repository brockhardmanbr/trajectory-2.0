//
//  TRJTimelineTableViewController.h
//  Trajectory
//
//  Created by Brock Hardman on 5/2/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

@protocol TRJTimelineTableViewControllerDelegate <NSObject>

- (void)userDidLongPressTimelineWithLongPress:(UILongPressGestureRecognizer *)longPress fromTableView:(UITableView *)tableView;

@end

@interface TRJTimelineTableViewController : UITableViewController

@property (nonatomic) id<TRJTimelineTableViewControllerDelegate> delegate;

+ (NSString *)storyboardID;

@end

