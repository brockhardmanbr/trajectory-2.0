//
//  TRJUserObject.m
//  Trajectory
//
//  Created by Michael O'Brien on 5/21/14.
//  Copyright (c) 2014 Bottle Rocket, LLC. All rights reserved.
//

#import "TRJUser.h"

@implementation TRJUser

static TRJUser *currentUser;

+ (TRJUser *)currentUser
{
    return currentUser;
}

+ (void)setCurrentUser:(TRJUser *)currentUserObject
{
    currentUser = currentUserObject;
}

+(NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"identifier": @"id",
             @"firstName": @"firstName",
             @"lastName": @"lastName",
             @"emailAddress": @"email",
             @"profileFileURL": @"avatar_url"};
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"%@ (%@ %@)", self.emailAddress, self.firstName, self.lastName];
}

- (NSString *)fullName
{
    NSString *fullName = [NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName];
    return fullName;
}

@end
