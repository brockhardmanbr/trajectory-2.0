//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

@protocol TRJUserImageProvider <NSObject>

- (void)loadStillCameraAssets;

@end
