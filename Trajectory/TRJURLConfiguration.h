//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TRJPostsConfiguration.h"
#import "TRJProfileConfiguration.h"

@interface TRJURLConfiguration : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) TRJPostsConfiguration *postsConfiguration;
@property (nonatomic, strong) TRJProfileConfiguration *profileConfiguration;

@end
