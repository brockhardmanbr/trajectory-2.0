//
//  TRJDataServiceProvider.h
//  Trajectory
//
//  Created by Brock Hardman on 4/30/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import "TRJDataServiceProviderProtocol.h"

@interface TRJDataServiceProvider : NSObject <TRJDataServiceProviderProtocol>

@end
