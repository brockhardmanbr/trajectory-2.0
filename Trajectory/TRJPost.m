//
//  TRJPostObject.m
//  Trajectory
//
//  Created by Michael O'Brien on 5/21/14.
//  Copyright (c) 2014 Bottle Rocket, LLC. All rights reserved.
//

#import "TRJPost.h"

@interface TRJPost ()

@property (nonatomic, strong) NSMutableArray *mutableViewedBy;

@end

@implementation TRJPost

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"identifier": @"id",
             @"timeStamp": @"postTimeStamp",
             @"postUser": @"user",
             @"postContent": @"post_contents",
             @"comments": @"comments",
             @"withUsers": @"withUsers",
             @"viewedBy": @"viewedBy",
             @"venueID": @"venueID",
             @"text": @"text",
             };
}

+ (NSValueTransformer *)postUserJSONTransformer
{
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:[TRJUser class]];
}

+ (NSValueTransformer *)postContentJSONTransformer
{
    return  [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:[TRJPostContent class]];
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"\nPost %d\tPosted By: %@ (%@ %@)\tPosted On: %@\nPost: %@\n\n",(int)self.identifier, self.postUser.emailAddress, self.postUser.firstName, self.postUser.lastName, self.timeStamp, self.text];
}

- (void)markPostAsSeenByUser:(TRJUser *)user
{
    if (![self.mutableViewedBy containsObject:user])
    {
        [self.mutableViewedBy addObject:user];
    }
}

- (TRJPostContentType)getContentTypesBitmask
{
    TRJPostContentType contentType = TRJPostContentTypeNone;
    
    for (TRJPostContent *contentTypeObject in self.postContent)
    {
        contentType = (contentType | [contentTypeObject contentType]);
    }
    
    if ([self.comments count] > 0)
    {
        contentType = (contentType | TRJPostContentTypeComment);
    }
    
    return contentType;
}

- (NSArray *)viewedBy
{
    return [NSArray arrayWithArray:self.mutableViewedBy];
}

- (void)setViewedBy:(NSArray *)viewedBy
{
    self.mutableViewedBy = [NSMutableArray arrayWithArray:viewedBy];
}

- (NSInteger)getHeartNumber
{
    NSInteger heartEmoticonCount = 0;
    for (TRJPostContent *content in self.postContent) {
        if (content.contentType == TRJPostContentTypeEmoticon) {
            heartEmoticonCount++;
        }
    }
    return heartEmoticonCount;
}
- (NSString *)getPostContentFilePath
{
    for (TRJPostContent *content in self.postContent) {
        if (content.contentType == TRJPostContentTypeImage) {
            return content.filePath;
        }
    }
    return nil;
}

@end
