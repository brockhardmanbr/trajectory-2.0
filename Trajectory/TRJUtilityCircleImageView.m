//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "TRJUtilityCircleImageView.h"
#import "UIImageView+AFNetworking.h"

@interface TRJUtilityCircleImageView()

@property (nonatomic) IBInspectable CGSize imageSize;
@property (nonatomic, strong) UIImageView *imageViewInside;
@end

@implementation TRJUtilityCircleImageView

- (void)prepareForInterfaceBuilder
{
    [super prepareForInterfaceBuilder];
    
    [self updateImage];
}

- (void)updateImage
{
    [super updateMask];
    
    if (self.image)
    {
        [self updateWithImage:self.image imageSize:self.imageSize];
    }
}

- (void)setImageURL:(NSURL *)URL
{
	[self updateImage];
	if (!self.imageViewInside)
	{
		[self updateWithImage:self.image imageSize:self.imageSize];
	}
	[self.imageViewInside setImageWithURL:URL placeholderImage:self.image];
}

- (void)updateWithImage:(UIImage *)image imageSize:(CGSize)size
{
    CGFloat beginX = (self.frame.size.width / 2) - (size.width / 2);
    CGFloat beginY =(self.frame.size.height / 2) - (size.height / 2);
    CGRect frame = CGRectMake(beginX, beginY, size.width, size.height);
	if (!self.imageViewInside)
	{
		self.imageViewInside = [[UIImageView alloc] initWithFrame:frame];
		[self addSubview:self.imageViewInside];
	}
	
    self.imageViewInside.image = image;
}

@end
