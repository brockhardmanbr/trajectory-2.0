//
//  BRAImageUtility.h
//  CCFS
//
//  Created by Brian Drell on 4/11/14.
//  Copyright (c) 2014 Bottle Rocket, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^BRAImageUtilityCompletionBlock)(UIImage *image);

@interface BRAImageUtility : NSObject

+ (void)applyExtraLightEffectToImage:(UIImage *)image completion:(BRAImageUtilityCompletionBlock)block;
+ (void)applyExtraLightEffectToView:(UIView *)view inRect:(CGRect)rect completion:(BRAImageUtilityCompletionBlock)block;
+ (void)applyExtraLightEffectToKeyWindowWithCompletion:(BRAImageUtilityCompletionBlock)block;
+ (void)applyExtraLightEffectToImageWithURL:(NSURL *)url completion:(BRAImageUtilityCompletionBlock)block;

+ (void)applyLightEffectToImage:(UIImage *)image completion:(BRAImageUtilityCompletionBlock)block;
+ (void)applyLightEffectToView:(UIView *)view inRect:(CGRect)rect completion:(BRAImageUtilityCompletionBlock)block;
+ (void)applyLightEffectToKeyWindowWithCompletion:(BRAImageUtilityCompletionBlock)block;
+ (void)applyLightEffectToImageWithURL:(NSURL *)url completion:(BRAImageUtilityCompletionBlock)block;

+ (void)applyDarkEffectToImage:(UIImage *)image completion:(BRAImageUtilityCompletionBlock)block;
+ (void)applyDarkEffectToView:(UIView *)view inRect:(CGRect)rect completion:(BRAImageUtilityCompletionBlock)block;
+ (void)applyDarkEffectToKeyWindowWithCompletion:(BRAImageUtilityCompletionBlock)block;
+ (void)applyDarkEffectToImageWithURL:(NSURL *)url completion:(BRAImageUtilityCompletionBlock)block;

+ (UIImage *)croppedImage:(UIImage *)image inRect:(CGRect)cropRect;
+ (UIImage *)renderedView:(UIView *)view;
+ (UIImage *)renderedView:(UIView *)view croppedToRect:(CGRect)cropRect;

+ (UIImage *)image:(UIImage *)image WithSaturation:(CGFloat)saturation;

@end
