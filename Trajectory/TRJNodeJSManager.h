//
//  TRJNodeJSManager.h
//  Trajectory
//
//  Created by Brock Hardman on 4/29/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TRJNodeJSManager : NSObject

+ (instancetype)sharedManager;
- (NSArray *)configureWithJsonURLInternal;
- (NSArray *)configureWithJsonURLExternal:(NSString *)url;

@end
