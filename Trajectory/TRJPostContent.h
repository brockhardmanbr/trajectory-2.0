//
//  TRJPostContent.h
//  Trajectory
//
//  Created by Brock Hardman on 5/22/14.
//  Copyright (c) 2014 Bottle Rocket, LLC. All rights reserved.
//

#import "MTLModel.h"
#import "TRJPostMetaData.h"
#import <Mantle/Mantle.h>

typedef NS_OPTIONS(NSInteger, TRJPostContentType) {
    TRJPostContentTypeNone = 1 << 0,
    TRJPostContentTypeEmoticon = 1 << 1,
    TRJPostContentTypeText = 1 << 2,
    TRJPostContentTypeImage = 1 << 3,
    TRJPostContentTypeComment = 1 << 4,
    TRJPostContentTypeShoutOut = 1 << 5,
    TRJPostContentTypeLocation = 1 << 6
};


@interface TRJPostContent : MTLModel<MTLJSONSerializing>

@property (nonatomic) NSInteger identifier;
@property (nonatomic) TRJPostContentType contentType;
@property (nonatomic) NSString *contentTypeString;
@property (nonatomic, copy) NSString *postText;
@property (nonatomic, strong) NSArray *shoutoutUsers;
@property (nonatomic, strong) TRJPostMetaData *postMetaData;
@property (nonatomic) NSInteger postIdentifier;
@property (nonatomic, strong) NSString *filePath;

- (TRJPostContentType)contentType;
- (NSString *)filePath;

@end

