//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

@protocol TRJUserVideoProvider <NSObject>

- (void)loadVideoCameraAssets;

@end
