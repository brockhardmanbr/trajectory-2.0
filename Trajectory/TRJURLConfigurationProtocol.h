//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

// This protocol provides URLs for the customer space.

@protocol TRJURLConfigurationProtocol <NSObject>

// fetch base configuration
- (void)fetchConfigurationWithCompletion:(void (^)(id configuration, NSError *error))completion;

// Posts API endpoints
- (NSURL *)postsBaseURL;
- (NSString *)allPostsEndpoint;
- (NSString *)singlePostEndpoint;
- (NSString *)createPostEndpoint;
- (NSString *)deletePostEndpoint;
- (NSString *)postContentEndpoint;
- (NSString *)postMetaDataEndpoint;

// Profile endpoints
- (NSURL *)profileBaseURL;
- (NSString *)profileEndpoint;

@end
