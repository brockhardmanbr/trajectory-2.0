//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "TRJUtilityAlertViewCustomAction.h"

@interface TRJUtilityAlertViewCustomAction()

@property (nonatomic, readwrite) NSString *actionTitle;
@property (nonatomic, readwrite) AlertActionStyleCustom actionStyle;
@property (nonatomic, readwrite) PSUtilityAlertViewCustomActionHandler actionHandler;

@end

@implementation TRJUtilityAlertViewCustomAction

+ (instancetype)actionWithTitle:(NSString *)title style:(AlertActionStyleCustom)style handler:(void (^)(TRJUtilityAlertViewCustomAction *action))handler
{
    TRJUtilityAlertViewCustomAction *action = [[self alloc] init];
    action.actionTitle = title;
    action.actionStyle = style;
    action.actionHandler = handler;
    
    return action;
}

@end
