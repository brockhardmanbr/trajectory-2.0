//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Blur)

- (UIImage *)blurredScreenshotLightImage;
- (void)addVisualEffectView;

@end
