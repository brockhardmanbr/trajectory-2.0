//
//  TRJTimelineTableViewController.m
//  Trajectory
//
//  Created by Brock Hardman on 5/2/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import "TRJTimelineTableViewController.h"
#import "TRJTimelineTableViewCell.h"
#import <Parse/Parse.h>
#import "TRJParsePost.h"

static NSString * const kTimelineCellIdentifier = @"timelineCell";

@interface TRJTimelineTableViewController ()

@property (nonatomic, strong) NSArray *timelinePosts;

@end

@implementation TRJTimelineTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self getAllPosts];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100.0;
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 60, 0);
        
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(userDidLongPress:)];
    longPress.minimumPressDuration = 0.75f;
    [self.tableView addGestureRecognizer:longPress];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TRJTimelineTableViewCell" bundle:nil] forCellReuseIdentifier:kTimelineCellIdentifier];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidAddPost:) name:kUserDidPostMessageNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidLogin:) name:kUserDidLoginNotification object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.tableView reloadData];
}

+ (NSString *)storyboardID
{
    return NSStringFromClass([self class]);
}

- (void)getAllPosts
{
    PFQuery *query = [PFQuery queryWithClassName:@"Posts"];
    
    __weak __typeof(self)weakSelf = self;
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error)
        {
            if ([objects isKindOfClass:[NSArray class]])
            {
                weakSelf.timelinePosts = [weakSelf convertObjectsToLocalModel:objects];
                [weakSelf.tableView reloadData];
            }
        }
        else
        {
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
}

- (NSArray *)convertObjectsToLocalModel:(NSArray *)objects
{
    NSMutableArray *mutable = [[NSMutableArray alloc] initWithCapacity:[objects count]];
    for (PFObject *object in objects)
    {
        TRJParsePost *parsePost = [[TRJParsePost alloc] init];
        parsePost.objectId = object.objectId;
        parsePost.updatedAt = object.updatedAt;
        parsePost.createdAt = object.createdAt;
        parsePost.postText = [object objectForKey:@"postText"];
        parsePost.userId = [object objectForKey:@"userId"];
        //parsePost.postImages = object.;
        [mutable addObject:parsePost];
    }
    
    return [self postArrayOrderedByDateMostRecent:[mutable copy]];
}

- (NSArray *)postArrayOrderedByDateMostRecent:(NSArray *)array
{
    if ([array count])
    {
        NSArray *sortedArray = [array sortedArrayUsingComparator:^NSComparisonResult(TRJParsePost *obj1, TRJParsePost *obj2) {
            return [obj2.createdAt compare:obj1.createdAt];
        }];
        
        return sortedArray;
    }
    
    return @[];
}

- (void)userDidLogin:(NSNotification *)notification
{
    [self getAllPosts];
}

- (void)hideOrShowContentForCellAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - Gesture Recognizers

- (void)userDidLongPress:(UILongPressGestureRecognizer *)longPress
{
    if (longPress)
    {
        [self.delegate userDidLongPressTimelineWithLongPress:longPress fromTableView:self.tableView];
    }
}

#pragma mark - Notifications

- (void)userDidAddPost:(NSNotification *)notification
{
    [self getAllPosts];
}

#pragma mark - TableView Datasource and Delegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.timelinePosts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TRJTimelineTableViewCell *cell = (TRJTimelineTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kTimelineCellIdentifier forIndexPath:indexPath];
    TRJParsePost *post = self.timelinePosts[indexPath.row];
    [cell configureWithPost:post];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self hideOrShowContentForCellAtIndexPath:indexPath];
    
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView endUpdates];
}

@end
