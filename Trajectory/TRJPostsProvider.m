//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "TRJPostsProvider.h"
#import "TRJPost.h"
#import "TRJPostContent.h"
#import "TRJPostMetaData.h"
#import "TRJURLConfigurationProtocol.h"

@interface TRJPostsProvider()

@end

@implementation TRJPostsProvider

+ (NSDictionary *)modelClassesByResourcePath
{
	id <TRJURLConfigurationProtocol> configuration = [[BRAFServiceLocator sharedLocator] serviceForProtocol:@protocol(TRJURLConfigurationProtocol)];
	return @{
                [configuration singlePostEndpoint] : [TRJPost class],
                [configuration createPostEndpoint] : [TRJPost class],
                [configuration deletePostEndpoint] : [TRJPost class],
                [configuration postContentEndpoint] : [TRJPostContent class],
                [configuration postMetaDataEndpoint] : [TRJPostMetaData class]
			 };
}

- (void)fetchAllPostsWithCompletion:(void (^)(NSArray *, NSError *))completion
{
    
}

- (instancetype)initWithConfiguredBaseURL
{
	id <TRJURLConfigurationProtocol> configuration = [[BRAFServiceLocator sharedLocator] serviceForProtocol:@protocol(TRJURLConfigurationProtocol)];
	NSURL *baseURL = [configuration postsBaseURL];
	
	self = [super initWithBaseURL:baseURL sessionConfiguration:nil];
	if (self)
	{
		
	}
	return self;
}

- (id <TRJURLConfigurationProtocol>)configuration
{
	id <TRJURLConfigurationProtocol> configuration = [[BRAFServiceLocator sharedLocator] serviceForProtocol:@protocol(TRJURLConfigurationProtocol)];
	return configuration;
}

- (NSArray *)mergeArrays:(NSArray *)arrayOfArrays
{
	// NB these should be sorted by date or something like that.
	NSArray *allItemsArray = @[];
	for (NSArray *array in arrayOfArrays)
	{
		allItemsArray = [allItemsArray arrayByAddingObjectsFromArray:array];
	}
	return allItemsArray;
}

- (void)fetchAllMomentsWithCompletion:(void (^)(NSArray *allMoments, NSError *error))completion
{
	// NB THere's certain to be a better & safer way to do this.
	NSAssert(completion, @"Fetch without completion is a dead end");

	__block NSArray *appointmentArray;
	__block NSArray *curatedArray;
	__block NSArray *offerArray;
	__block NSArray *photoArray;
	__block NSArray *textArray;

	[self fetchPhotoMomentsWithCompletion:^(NSArray *photoMoments, NSError *error) {
		
		photoArray = photoMoments ? photoMoments : @[];
		
		if (appointmentArray && curatedArray && offerArray && photoArray && textArray)
		{
			if (completion)
			{
				completion([self mergeArrays:@[appointmentArray, curatedArray, offerArray, photoArray, textArray]], nil);
			}
		}
	}];

	[self fetchTextMomentsWithCompletion:^(NSArray *textMoments, NSError *error) {
		
		textArray = textMoments ? textMoments : @[];
		
		if (appointmentArray && curatedArray && offerArray && photoArray && textArray)
		{
			if (completion)
			{
				completion([self mergeArrays:@[appointmentArray, curatedArray, offerArray, photoArray, textArray]], nil);
			}
		}
	}];
}

- (void)fetchPhotoMomentsWithCompletion:(void (^)(NSArray *, NSError *))completion
{
//	NSAssert(completion, @"Fetch without completion is a dead end");
//	[self GET:[[self configuration] photoMomentsEndpoint] parameters:nil completion:^(OVCResponse  * response, NSError *error) {
//		NSArray *array = response.result;
//		if (completion)
//		{
//			completion(array, error);
//		}
//	}];
}

- (void)fetchTextMomentsWithCompletion:(void (^)(NSArray *, NSError *))completion
{
//	NSAssert(completion, @"Fetch without completion is a dead end");
//	[self GET:[[self configuration] textMomentsEndpoint] parameters:nil completion:^(OVCResponse  * response, NSError *error) {
//		NSArray *array = response.result;
//		if (completion)
//		{
//			completion(array, error);
//		}
//	}];
}

@end
