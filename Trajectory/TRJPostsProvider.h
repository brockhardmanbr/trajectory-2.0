//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "TRJPostsProviderProtocol.h"

@interface TRJPostsProvider : OVCHTTPSessionManager <TRJPostsProviderProtocol>

// preferred constructor
- (instancetype)initWithConfiguredBaseURL NS_DESIGNATED_INITIALIZER;

@end
