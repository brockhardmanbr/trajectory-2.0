//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

@interface UIColor (TRJ)

// Primary brand colors
+ (instancetype)pet_primBlue;
+ (instancetype)pet_primRed;
+ (instancetype)pet_primWhite;
+ (instancetype)pet_primGrayDrk;

// Secondary app colors
+ (instancetype)pet_secBlueDrk;
+ (instancetype)pet_secBlueLt;
+ (instancetype)pet_secGray;
+ (instancetype)pet_secGrayLt;
+ (instancetype)pet_secPinkDrk;

// Timeline colors
+ (instancetype)pet_tiBlue;
+ (instancetype)pet_tiBlueLt;
+ (instancetype)pet_tiBlueGreen;
+ (instancetype)pet_tiGreenLt;
+ (instancetype)pet_tiYellow;
+ (instancetype)pet_tiOrange;
+ (instancetype)pet_tiRedLt;
+ (instancetype)pet_tiPurpl;

//Profile colors
+ (instancetype)pet_profRed;
+ (instancetype)pet_profLightBlue;
+ (instancetype)pet_profDarkBlue;
+ (instancetype)pet_profLightGreen;
@end

