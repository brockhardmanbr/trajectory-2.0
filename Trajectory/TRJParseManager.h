//
//  TRJParseManager.h
//  Trajectory
//
//  Created by Brock Hardman on 4/30/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TRJParseManager : NSObject

+ (instancetype)sharedManager;
- (NSArray *)configurePostOptionsFromJSON:(NSString *)jsonFile;
- (NSArray *)configureWithJsonURLExternal:(NSString *)url;

@end
