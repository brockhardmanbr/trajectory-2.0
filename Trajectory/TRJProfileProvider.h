//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "TRJProfileProtocol.h"

@interface TRJProfileProvider : OVCHTTPSessionManager <TRJProfileProtocol>

- (instancetype)initWithConfiguredBaseURL NS_DESIGNATED_INITIALIZER;

@end
