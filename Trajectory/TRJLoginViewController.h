//
//  TRJLoginViewController.h
//  Trajectory
//
//  Created by Brock Hardman on 4/30/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import "TRJLoginControllerProtocol.h"

typedef void(^TRJLoginViewControllerCompletionBlock)(void);

typedef NS_OPTIONS(NSInteger, TRJLoginControllerProviderServiceType)
{
    TRJLoginControllerProviderServiceTypeNodeJS,
    TRJLoginControllerProviderServiceTypeParse
};

@interface TRJLoginViewController : UIViewController <TRJLoginControllerProtocol>

- (void)configureWithService:(TRJLoginControllerProviderServiceType)service withCompletionBlock:(TRJLoginViewControllerCompletionBlock)completionBlock;
+ (NSString *)storyboardIdentifier;

@end
