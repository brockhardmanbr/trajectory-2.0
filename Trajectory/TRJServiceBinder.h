//
//  TRJServiceBinder.h
//  Trajectory
//
//  Created by Brock Hardman on 3/4/15.
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TRJServiceBinder : NSObject

+ (void)bindServices;

@end
