//
//  TRJActiveViewController.h
//  Trajectory
//
//  Created by Brock Hardman on 3/30/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TRJActiveViewControllerProtocol <NSObject>

- (void)configureWithNavigationTitle:(NSString *)navigationTitle navigationImage:(NSString *)navigationImage;

@end
