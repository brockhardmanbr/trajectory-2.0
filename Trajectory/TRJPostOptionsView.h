//
//  TRJPostOptionsView.h
//  Trajectory
//
//  Created by Brock Hardman on 5/3/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

@interface TRJPostOptionsView : UIView

- (void)configureWithOptions:(NSArray *)options;

@end
