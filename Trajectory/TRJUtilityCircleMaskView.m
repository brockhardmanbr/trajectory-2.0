//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "TRJUtilityCircleMaskView.h"

@interface TRJUtilityCircleMaskView()

@property (nonatomic, strong) IBInspectable UIColor *borderColor;
@property (nonatomic, strong) IBInspectable UIColor *fillColor;
@property (nonatomic) IBInspectable CGFloat borderWidth;

@end

@implementation TRJUtilityCircleMaskView

- (void)prepareForInterfaceBuilder
{
    [super prepareForInterfaceBuilder];
    
    [self updateMask];
}

- (void)updateMask
{
    [self updateWithBorderColor:self.borderColor borderWidth:self.borderWidth fillColor:self.fillColor];
}

- (void)updateWithBorderColor:(UIColor *)borderColor borderWidth:(CGFloat)borderWidth fillColor:(UIColor *)fillColor
{
    [self addCircleMaskWithBorderColor:borderColor borderWidth:borderWidth fillColor:fillColor];
}

@end
