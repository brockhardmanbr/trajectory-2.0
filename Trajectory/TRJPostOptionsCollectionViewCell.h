//
//  TRJPostOptionsCollectionViewCell.h
//  Trajectory
//
//  Created by Brock Hardman on 5/3/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import "TRJPostOption.h"

@interface TRJPostOptionsCollectionViewCell : UICollectionViewCell

- (void)configureWithPostOption:(TRJPostOption *)option;

@end
