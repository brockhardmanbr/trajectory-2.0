//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "TRJURLConfigurationProvider.h"
#import "TRJURLConfiguration.h"
#import "TRJPostsConfiguration.h"
#import "TRJProfileConfiguration.h"

static NSString * const kBaseConfigurationURL = @"http://private-41a5-brpetsmart.apiary-mock.com";

@interface TRJURLConfigurationProvider()

@property (nonatomic, strong) TRJPostsConfiguration *postsConfiguration;
@property (nonatomic, strong) TRJProfileConfiguration *profileConfiguration;

@end

@implementation TRJURLConfigurationProvider

- (instancetype)initWithDefaultURL
{
	self = [super initWithBaseURL:[NSURL URLWithString:kBaseConfigurationURL] sessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
	return self;

}

+ (NSDictionary *)modelClassesByResourcePath
{
	return @{
			 @"config": [TRJURLConfiguration class],
			 };
}

#pragma mark - fetch configuration

- (void)fetchConfigurationWithCompletion:(void (^)(id configuration, NSError * error))completion
{
//	[self GET:@"/customerconfig" parameters:nil completion:^(AFURLResponseSerialization  *response, NSError *error) {
//		TRJURLConfiguration *configuration = response.result;
//		self.postsConfiguration = configuration.postsConfiguration;
//		self.profileConfiguration = configuration.profileConfiguration;
//		if (completion)
//		{
//			completion(configuration,error);
//		}
//	}];
}

#pragma mark - posts endpoints

- (NSURL *)postsBaseURL
{
	// URLWithString throws an exception if nil, so check first.
	if (self.postsConfiguration.baseURLString)
	{
		return [NSURL URLWithString:self.postsConfiguration.baseURLString];
	}
	return nil;
}

- (NSString *)allPostsEndpoint
{
	return self.postsConfiguration.allPostsEndpoint;
}

- (NSString *)singlePostEndpoint;
{
	return self.postsConfiguration.singlePostEndpoint;
}

- (NSString *)createPostEndpoint
{
	return self.postsConfiguration.createPostEndpoint;
}

- (NSString *)deletePostEndpoint
{
	return self.postsConfiguration.deletePostEndpoint;
}

- (NSString *)postContentEndpoint
{
	return self.postsConfiguration.contentEndpoint;
}

- (NSString *)postMetaDataEndpoint
{
	return self.postsConfiguration.metadataEndpoint;
}

#pragma mark - profile endpoints

- (NSURL *)profileBaseURL
{
    return nil;
}

- (NSString *)profileEndpoint
{
    return nil;
}

@end
