//
//  TRJGlobalNavRefreshProtocol.h
//  Trajectory
//
//  Created by Brock Hardman on 5/2/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

@protocol TRJGlobalNavRefreshProtocol <NSObject>

@optional
- (void)updateNavigationLayout;

@end
