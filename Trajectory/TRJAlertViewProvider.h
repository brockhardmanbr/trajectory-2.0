//
//  AlertViewProvider.h
//  
//
//  Created by Brock Hardman on 1/26/15.
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

@protocol TRJAlertViewProvider <NSObject>

- (void)showCustomAlert;

@end
