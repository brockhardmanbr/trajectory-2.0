//
//  TRJUserListTableViewCell.h
//  Trajectory
//
//  Created by Brock Hardman on 5/1/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

@class TRJParseUser;

@interface TRJUserListTableViewCell : UITableViewCell

- (void)configureWithUser:(TRJParseUser *)user;
- (CGSize)cellSize;

@end
