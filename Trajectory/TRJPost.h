//
//  TRJPostObject.h
//  Trajectory
//
//  Created by Michael O'Brien on 5/21/14.
//  Copyright (c) 2014 Bottle Rocket, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TRJUser.h"
#import "TRJPostContent.h"
#import <Mantle/Mantle.h>

@interface TRJPost : MTLModel <MTLJSONSerializing>

@property (nonatomic) NSInteger identifier;
@property (nonatomic, copy) NSString *timeStamp;
@property (nonatomic, strong) TRJUser *postUser;
@property (nonatomic, copy) NSString *text;
@property (nonatomic, strong) NSArray *postContent;
@property (nonatomic, strong) NSArray *comments;
@property (nonatomic, strong) NSArray *withUsers;
@property (nonatomic, copy) NSString *venueID;
@property (nonatomic, strong) NSArray *viewedBy;

- (void)markPostAsSeenByUser:(TRJUser *)user;
- (NSInteger)getHeartNumber;

- (TRJPostContentType)getContentTypesBitmask;
- (NSString *)getPostContentFilePath;

@end
