//
//  TRJTimelineTableViewCell.m
//  Trajectory
//
//  Created by Brock Hardman on 5/1/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import "TRJTimelineTableViewCell.h"
#import "TRJUtilityCircleImageView.h"
#import "TRJParsePost.h"
#import <Parse/Parse.h>

@interface TRJTimelineTableViewCell()

@property (weak, nonatomic) IBOutlet TRJUtilityCircleImageView *circleImageView;
@property (weak, nonatomic) IBOutlet UICollectionView *photosCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *circleImageViewConstraintWidth;
@property (nonatomic) CGFloat heightForPhotosView;
@property (nonatomic, strong) TRJParsePost *post;

@end

@implementation TRJTimelineTableViewCell

- (void)configureWithPost:(TRJParsePost *)post
{
    self.post = post;
    
    [self.circleImageView updateImage];
    [self getUserInfoWithUserId:post.userId];
    [self configureWithText:post.postText];
    [self configureWithImages:post.postImages];
    
    [self layoutIfNeeded];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    [self cleanupCell];
}

- (void)getUserInfoWithUserId:(NSString *)userId
{
    PFQuery *query = [PFUser query];
    [query whereKey:@"objectId" equalTo:userId];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error)
        {
            NSLog(@"Error Getting user: %@", error.localizedDescription);
        }
        else
        {
            if ([objects count] && [[objects lastObject] isKindOfClass:[PFObject class]])
            {
                PFObject *object = (PFObject *)[objects lastObject];
                NSString *objectIdString = [object valueForKey:@"name"];
                NSArray *firstLast = [objectIdString componentsSeparatedByString: @" "];
                NSString *first = [[[firstLast objectAtIndex:0] substringToIndex:1] uppercaseString];
                NSString *last = [[[firstLast objectAtIndex:1] substringToIndex:1] uppercaseString];
                [self configureImageViewWithString:[NSString stringWithFormat:@"%@%@", first, last]];
            }
        }
            
    }];
}

- (void)configureWithText:(NSString *)text
{
    self.contentLabel.text = text;
}

- (void)configureImageViewWithString:(NSString *)string
{
    UIView *initialsView = [[UIView alloc] initWithFrame:self.circleImageView.bounds];
    UILabel *initialsLabel = [[UILabel alloc] initWithFrame:initialsView.frame];
    initialsLabel.text = string;
    initialsLabel.textAlignment = NSTextAlignmentCenter;
    initialsLabel.textColor = [UIColor whiteColor];
    [initialsView addSubview:initialsLabel];
    [self.circleImageView addSubview:initialsView];
}

- (void)configureWithImages:(NSArray *)images
{
    if ([images count])
    {
        
    }
    else
    {
        self.photosCollectionView = nil;
    }
}

- (void)cleanupCell
{
    self.photosCollectionView = nil;
    self.contentLabel.text = @"";
    self.circleImageView.image = nil;
}

@end
