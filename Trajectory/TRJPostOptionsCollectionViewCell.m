//
//  TRJPostOptionsCollectionViewCell.m
//  Trajectory
//
//  Created by Brock Hardman on 5/3/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import "TRJPostOptionsCollectionViewCell.h"
#import "TRJUtilityCircleImageView.h"
#import "TRJPostOptionsView.h"

@interface TRJPostOptionsCollectionViewCell()
@property (weak, nonatomic) IBOutlet UILabel *optionLabel;
@property (weak, nonatomic) IBOutlet TRJUtilityCircleImageView *optionImageTopView;
@property (nonatomic, strong) TRJPostOption *postOption;

@end

@implementation TRJPostOptionsCollectionViewCell

- (void)configureWithPostOption:(TRJPostOption *)option
{
    self.postOption = option;
    self.optionLabel.text = option.postTitle;
    self.optionImageTopView.image = [UIImage imageNamed:option.postImage];
    [self.optionImageTopView updateImage];
}

@end
