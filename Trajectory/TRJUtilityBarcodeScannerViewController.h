//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

@import AVFoundation;

typedef NS_ENUM(NSInteger, CameraBarcodeScannerViewControllerState)
{
    CameraBarcodeScannerViewControllerStateDefault
};

typedef void(^CameraCompletionBlock)(UIImage *image);

@protocol TRJCameraBarcodeScannerViewControllerDelegate <NSObject>

- (void)scanViewController:(UIViewController *)viewController didScanCardWithNumber:(NSString *)number type:(NSString *)metadataType;
- (void)scanViewController:(UIViewController *)viewController didReturnErrorWithObject:(id)object;

@end

@interface TRJUtilityBarcodeScannerViewController : UIViewController

@property (nonatomic, weak) id<TRJCameraBarcodeScannerViewControllerDelegate> delegate;

@end
