//
//  TRJPostMetaData.h
//  Trajectory
//
//  Created by Brock Hardman on 5/22/14.
//  Copyright (c) 2014 Bottle Rocket, LLC. All rights reserved.
//

@interface TRJPostMetaData : NSObject

@property (nonatomic) NSInteger identifier;
@property (nonatomic, copy) NSString *filePath;
@property (nonatomic) NSInteger fileSize;

@end
