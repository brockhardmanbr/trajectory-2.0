//
//  TRJPostOptionsCollectionViewDatasource.h
//  Trajectory
//
//  Created by Brock Hardman on 5/3/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TRJPostOptionsCollectionViewDatasource : NSObject

- (instancetype)initWithCollectionView:(UICollectionView *)collectionView options:(NSArray *)options;

@end
