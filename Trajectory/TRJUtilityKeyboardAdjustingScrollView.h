//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIScrollView+KeyboardAdjusting.h"

@interface TRJUtilityKeyboardAdjustingScrollView : UIScrollView <UITextFieldDelegate, UITextViewDelegate>

- (void)contentSizeToFit;
- (BOOL)focusNextTextField;
- (void)scrollToActiveTextField;

@end