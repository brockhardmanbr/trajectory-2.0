//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "TRJUserImageProvider.h"
#import "TRJUserVideoProvider.h"

typedef NS_ENUM(NSInteger, TRJCameraType)
{
    TRJCameraTypeCamera,
    TRJCameraTypeRoll,
    TRJCameraTypeSavedPhotos,
    TRJCameraTypeBarcodeScanner
};

typedef NS_ENUM(NSInteger, TRJCameraState)
{
    TRJCameraStateStill,
    TRJCameraStateVideo
};

typedef void(^TRJCameraCompletionBlock)(UIImage *image);
typedef void(^TRJCameraMultiSelectCompletionBlock)(NSArray *assets);

@interface TRJUtilityCamera : NSObject <TRJUserImageProvider, TRJUserVideoProvider>

- (instancetype)initWithObject:(id)passedObject withInitialState:(TRJCameraState)state withCameraType:(TRJCameraType)type allowsMultiSelect:(BOOL)allowsMultiSelect withCompletionBlock:(id)completionBlock;
- (void)updateToNewState:(TRJCameraState)state withType:(TRJCameraType)type;

@end
