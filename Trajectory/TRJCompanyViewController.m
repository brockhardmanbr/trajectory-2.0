//
//  TRJScreen4ViewController.m
//  Trajectory
//
//  Created by Brock Hardman on 3/21/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import "TRJCompanyViewController.h"
#import <Parse/Parse.h>
#import "TRJParseUser.h"
#import "TRJUserListTableViewCell.h"

static NSString * const kUserListCellIdentifier = @"userListCell";

@interface TRJCompanyViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *users;

@end

@implementation TRJCompanyViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self getAllUsers];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TRJUserListTableViewCell" bundle:nil] forCellReuseIdentifier:kUserListCellIdentifier];
}

- (void)getAllUsers
{
    PFQuery *query = [PFUser query];
    
    __weak __typeof(self)weakSelf = self;
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error)
        {
            if ([objects isKindOfClass:[NSArray class]])
            {
                weakSelf.users = [weakSelf convertObjectsToLocalModel:objects];
                [weakSelf.tableView reloadData];
            }
        }
        else
        {
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
}

- (NSArray *)convertObjectsToLocalModel:(NSArray *)objects
{
    NSMutableArray *mutable = [[NSMutableArray alloc] initWithCapacity:[objects count]];
    for (PFUser *object in objects)
    {
        TRJParseUser *parseUser = [[TRJParseUser alloc] init];
        parseUser.objectId = object.objectId;
        parseUser.username = [object objectForKey:@"username"];
        parseUser.email = [object objectForKey:@"email"];
        parseUser.name = [object objectForKey:@"name"];
        parseUser.organizationalUnit = [object objectForKey:@"organizationalUnit"];
        parseUser.position = [object objectForKey:@"position"];
        
        [mutable addObject:parseUser];
    }
    
    return [mutable copy];
}

#pragma mark - TableView Datasource and Delegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.users count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TRJUserListTableViewCell *cell = (TRJUserListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kUserListCellIdentifier forIndexPath:indexPath];
    TRJParseUser *user = self.users[indexPath.row];
    [cell configureWithUser:user];
    
    return cell;
}

@end
