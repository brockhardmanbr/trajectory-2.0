//
//  Constants.h
//  Trajectory
//
//  Created by Brock Hardman on 3/4/15.
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

//Server Endpoints
static NSString * const kNodeJSServerBaseURL = @"http://localhost:3000/";
static NSString * const kNodeJSProfileEndpoint = @"profile";
static NSString * const kNodeJSTimelineEndpoint = @"timeline";
static NSString * const kNodeJSPostEndpoint = @"post";
static NSString * const kNodeJSFilesEndpoint = @"files";

//Config File View Controllers
static NSString * const kTRJConfigURL = @"http://sandbox.bottlerocketapps.com/Trajectory/config.json";

//TRJ Camera
static NSString * const kTRJCameraWithCameraContext = @"TRJCameraWithCamera";
static NSString * const kTRJCameraWithRollContext = @"TRJCameraWithRoll";
static NSString * const kTRJCameraWithMultiSelectRollContext = @"TRJCameraWithMultiSelectRoll";
static NSString * const kTRJCameraWithSavedPhotosContext = @"TRJCameraWithSavedPhotos";
static NSString * const kTRJCameraWithBarcodeScannerContext = @"TRJCameraWithBarcodeScanner";

//Alert View
static NSString * const kAlertViewCustomContext = @"TRJAlertViewCustom";

//Active Controllers
static NSString * const kActiveControllersContext = @"TRJActiveControllers";

//Login Controller
static NSString * const kLoginControllerContext = @"TRJLoginController";

//UIView Categories
static CGFloat const kRoundedCornerRadiusDefault = 4.0f;

//Notifications
static NSString * const kUserDidPostMessageNotification = @"kUserDidPostMessageNotification";
static NSString * const kUserDidLoginNotification = @"kUserDidLoginNotification";