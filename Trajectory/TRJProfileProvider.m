//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "TRJProfileProvider.h"
#import "TRJUser.h"
#import "TRJURLConfigurationProtocol.h"

@implementation TRJProfileProvider

+ (NSDictionary *)modelClassesByResourcePath
{
	id <TRJURLConfigurationProtocol> configuration = [[BRAFServiceLocator sharedLocator] serviceForProtocol:@protocol(TRJURLConfigurationProtocol)];
	NSString *endpoint = [[configuration profileEndpoint] stringByReplacingOccurrencesOfString:@"{id}" withString:@"*"];
	if (endpoint)
		return @{ endpoint : [TRJUser class] };
	return nil;
}

- (NSString *)shortDisplayName
{
    return @"Fake Display Name";
}

- (NSURL *)profileImageURL
{
    return [NSURL URLWithString:@"http://www.google.com"];
}

- (instancetype)initWithConfiguredBaseURL
{
	id <TRJURLConfigurationProtocol> configuration = [[BRAFServiceLocator sharedLocator] serviceForProtocol:@protocol(TRJURLConfigurationProtocol)];
	NSURL *baseURL = [configuration profileBaseURL];
	
	self = [super initWithBaseURL:baseURL sessionConfiguration:nil];
	if (self)
	{
		
	}
	return self;
}

@end
