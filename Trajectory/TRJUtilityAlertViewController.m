//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "TRJUtilityAlertViewController.h"

@interface TRJUtilityAlertViewController ()

@property (nonatomic, strong) NSArray *actions;
@property (nonatomic, strong) NSArray *textFields;
@property (nonatomic, readwrite) PSUtilityAlertControllerStyleCustom preferredStyle;

@end

@implementation TRJUtilityAlertViewController

+ (instancetype)alertViewControllerWithTitle:(NSString *)title message:(NSString *)message preferredStyle:(PSUtilityAlertControllerStyleCustom)preferredStyle backgroundColor:(UIColor *)backgroundColor
{
    TRJUtilityAlertViewController *alertController = [[self alloc] init];
    alertController.title = title;
    alertController.message = message;
    alertController.view.backgroundColor = backgroundColor;
    return alertController;
}

- (NSArray *)actions
{
    if (!_actions)
        _actions = [[NSArray alloc] init];
    return _actions;
}

- (void)addAction:(TRJUtilityAlertViewCustomAction *)action
{
	self.actions = [self.actions arrayByAddingObject:action];
}

- (void)placeActionButtons
{
    __block CGFloat beginBottom = self.view.frame.size.height - 20;
    CGFloat buttonHeight = 30;
	
	[self.actions enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		TRJUtilityAlertViewCustomAction *action = obj;
		NSAttributedString *attrStr = [[NSAttributedString alloc] initWithString:action.actionTitle];
		CGRect rect = [attrStr boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, buttonHeight) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) context:nil];
		CGFloat beginX = self.view.center.x - ((rect.size.width + 30) / 2);
		UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(beginX, beginBottom - buttonHeight, rect.size.width + 30, buttonHeight)];
		[button setTitle:action.actionTitle forState:UIControlStateNormal];
		[button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
		[button addTarget:self action:@selector(actionTapped:) forControlEvents:UIControlEventTouchUpInside];
		button.layer.cornerRadius = 3;
		button.layer.borderWidth = 1;
		button.layer.borderColor = [UIColor whiteColor].CGColor;
		button.tag = idx;
		
		[self.view addSubview:button];
		
		beginBottom -= buttonHeight * 2;
	}];
	
}

- (void)actionTapped:(UIButton *)button
{
    NSUInteger tag = button.tag;
    TRJUtilityAlertViewCustomAction *action = [self.actions objectAtIndex:tag];
    if (action.actionHandler)
    {
        action.actionHandler(action);
    }
}

- (void)addTextFieldWithConfigurationHandler:(void (^)(UITextField *textField))configurationHandler
{
    
}

@end
