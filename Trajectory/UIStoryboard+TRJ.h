//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIStoryboard (TRJ)

+ (UIStoryboard *)trjProfileStoryboard;

@end
