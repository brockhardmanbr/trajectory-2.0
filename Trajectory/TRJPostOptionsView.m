//
//  TRJPostOptionsView.m
//  Trajectory
//
//  Created by Brock Hardman on 5/3/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import "TRJPostOptionsView.h"
#import "TRJPostOptionsCollectionViewDatasource.h"

@interface TRJPostOptionsView()

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) TRJPostOptionsCollectionViewDatasource *datasource;
@property (nonatomic, strong) NSArray *options;

@end

@implementation TRJPostOptionsView

- (void)configureWithOptions:(NSArray *)options
{
    self.options = options;
    self.layer.cornerRadius = 6;
    
    self.datasource = [[TRJPostOptionsCollectionViewDatasource alloc] initWithCollectionView:self.collectionView options:self.options];
}

@end
