//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "TRJDateFormatterProviderProtocol.h"

@interface TRJDateFormatterProvider : NSObject <TRJDateFormatterProviderProtocol>

@end
