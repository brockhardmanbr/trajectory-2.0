//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "UIColor+TRJ.h"

@implementation UIColor (TRJ)

// Primary brand colors
+ (instancetype)pet_primBlue
{
	return [UIColor colorWithRed:0.0/255.0f green:61.0/255.0f blue:165.0/255.0f alpha:1.0f];
}

+ (instancetype)pet_primRed
{
	return [UIColor colorWithRed:229.0/255.0f green:25.0/255.0f blue:55.0/255.0f alpha:1.0f];
}

+ (instancetype)pet_primWhite
{
	return [UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0f];
}

+ (instancetype)pet_primGrayDrk
{
	return [UIColor colorWithRed:54.0/255.0f green:54.0/255.0f blue:54.0/255.0f alpha:1.0f];
}

// Secondary app colors
+ (instancetype)pet_secBlueDrk
{
	return [UIColor colorWithRed:0.0/255.0f green:40.0/255.0f blue:100.0/255.0f alpha:1.0f];
}

+ (instancetype)pet_secBlueLt
{
	return [UIColor colorWithRed:43.0/255.0f green:169.0/255.0f blue:208.0/255.0f alpha:1.0f];
}

+ (instancetype)pet_secGray
{
	return [UIColor colorWithRed:204.0/255.0f green:204.0/255.0f blue:204.0/255.0f alpha:1.0f];
}

+ (instancetype)pet_secGrayLt
{
	return [UIColor colorWithRed:244.0/255.0f green:244.0/255.0f blue:244.0/255.0f alpha:1.0f];
}

+ (instancetype)pet_secPinkDrk
{
	return [UIColor colorWithRed:89.0/255.0f green:138.0/255.0f blue:211.0/255.0f alpha:1.0f];
}


// Timeline colors
+ (instancetype)pet_tiBlue
{
	return [UIColor colorWithRed:89.0/255.0f green:138.0/255.0f blue:211.0/255.0f alpha:1.0f];
}

+ (instancetype)pet_tiBlueLt
{
	return [UIColor colorWithRed:137.0/255.0f green:199.0/255.0f blue:224.0/255.0f alpha:1.0f];
}

+ (instancetype)pet_tiBlueGreen
{
	return [UIColor colorWithRed:48.0/255.0f green:162.0/255.0f blue:173.0/255.0f alpha:1.0f];
}

+ (instancetype)pet_tiGreenLt
{
	return [UIColor colorWithRed:158.0/255.0f green:191.0/255.0f blue:86.0/255.0f alpha:1.0f];
}

+ (instancetype)pet_tiYellow
{
	return [UIColor colorWithRed:253.0/255.0f green:204.0/255.0f blue:102.0/255.0f alpha:1.0f];
}

+ (instancetype)pet_tiOrange
{
	return [UIColor colorWithRed:251.0/255.0f green:118.0/255.0f blue:118.0/255.0f alpha:1.0f];
}

+ (instancetype)pet_tiRedLt
{
	return [UIColor colorWithRed:229.0/255.0f green:76.0/255.0f blue:110.0/255.0f alpha:1.0f];
}

+ (instancetype)pet_tiPurpl
{
	return [UIColor colorWithRed:160.0/255.0f green:121.0/255.0f blue:212.0/255.0f alpha:1.0f];
}

+ (instancetype)pet_timelineBlue
{
    return [UIColor colorWithRed:89/255.0f green:138/255.0f blue:211/255.0f alpha:1.0f];
}

+ (instancetype)pet_timelineBlueGreen
{
    return [UIColor colorWithRed:48/255.0f green:162/255.0f blue:173/255.0f alpha:1.0f];
}

//Profile colors
+ (instancetype)pet_profRed
{
    return [UIColor colorWithRed:206.0/255.0f green:0.0/255.0f blue:88.0/255.0f alpha:1.0f];
}

+ (instancetype)pet_profLightBlue
{
    return [UIColor colorWithRed:43.0/255.0f green:169.0/255.0f blue:208.0/255.0f alpha:1.0f];
}

+ (instancetype)pet_profDarkBlue
{
    return [UIColor colorWithRed:0.0/255.0f green:40.0/255.0f blue:100.0/255.0f alpha:1.0f];
}

+ (instancetype)pet_profLightGreen
{
    return [UIColor colorWithRed:206.0/255.0f green:220.0/255.0f blue:0.0/255.0f alpha:1.0f];
}

@end
