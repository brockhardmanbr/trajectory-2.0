//
//  TRJPostOptionsCollectionViewDatasource.m
//  Trajectory
//
//  Created by Brock Hardman on 5/3/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import "TRJPostOptionsCollectionViewDatasource.h"
#import "TRJPostOptionsCollectionViewCell.h"

static NSString * const kPostOptionsCollectionViewCellIdentifier = @"postOptionsCollectionViewCell";

@interface TRJPostOptionsCollectionViewDatasource() <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) NSArray *options;
@property (nonatomic, strong) UICollectionView *collectionView;

@end

@implementation TRJPostOptionsCollectionViewDatasource

- (instancetype)initWithCollectionView:(UICollectionView *)collectionView options:(NSArray *)options
{
    self = [super init];
    if (self)
    {
        _options = options;
        _collectionView = collectionView;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView registerNib:[UINib nibWithNibName:@"TRJPostOptionsCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:kPostOptionsCollectionViewCellIdentifier];
        
        [collectionView reloadData];
    }
    return self;
}

- (TRJPostOption *)getPostOptionForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *optionDictionary = [self.options objectAtIndex:indexPath.item];
    TRJPostOption *option = [[TRJPostOption alloc] init];
    option.postImage = [optionDictionary objectForKey:@"postTypeImage"];
    option.postTitle = [optionDictionary objectForKey:@"postTypeTitle"];
    option.postType = [optionDictionary objectForKey:@"postType"];
    
    return option;
}

#pragma mark - Collection View Datatsource/Delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.options count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TRJPostOptionsCollectionViewCell *cell = (TRJPostOptionsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kPostOptionsCollectionViewCellIdentifier forIndexPath:indexPath];
    TRJPostOption *option = [self getPostOptionForItemAtIndexPath:indexPath];
    [cell configureWithPostOption:option];
    
    return cell;
}

@end
