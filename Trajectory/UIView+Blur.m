//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "UIView+Blur.h"
#import "UIImage+ImageEffects.h"

@implementation UIView (Blur)

- (UIImage *)blurredScreenshotLightImage
{
    UIImage *screenshot = [self drawBackground];
    UIImage *blurredScreenshot = [screenshot applyLightEffect];
    return blurredScreenshot;
}

- (UIImage *)drawBackground
{
    UIGraphicsBeginImageContextWithOptions([[UIApplication sharedApplication] keyWindow].bounds.size, NO, 0.0);
    [[[UIApplication sharedApplication] keyWindow] drawViewHierarchyInRect:[[UIApplication sharedApplication] keyWindow].bounds afterScreenUpdates:NO];
    UIImage *imageRendered = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return imageRendered;
}

- (void)addVisualEffectView
{
    UIVisualEffect *blurEffect;
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    
    UIVisualEffectView *visualEffectView;
    visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    
    visualEffectView.frame = self.bounds;
    [self addSubview:visualEffectView];
}

@end
