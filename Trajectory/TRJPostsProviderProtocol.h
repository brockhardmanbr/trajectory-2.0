//
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TRJPostsProviderProtocol <NSObject>

- (void)fetchAllPostsWithCompletion:(void (^)(NSArray *posts, NSError *error))completion;

@end
