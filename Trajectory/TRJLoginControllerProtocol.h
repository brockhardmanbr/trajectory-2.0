//
//  TRJLoginControllerProtocol.h
//  Trajectory
//
//  Created by Brock Hardman on 4/30/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TRJLoginControllerProtocol <NSObject>

- (void)userDidLoginSuccessfully;
- (void)userDidFailLogin;

@end
