//
//  TRJTimelineTableViewCell.h
//  Trajectory
//
//  Created by Brock Hardman on 5/1/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

@class TRJParsePost;

@interface TRJTimelineTableViewCell : UITableViewCell

- (void)configureWithPost:(TRJParsePost *)post;

@end
